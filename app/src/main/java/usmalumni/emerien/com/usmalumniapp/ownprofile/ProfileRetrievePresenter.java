package usmalumni.emerien.com.usmalumniapp.ownprofile;

import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import usmalumni.emerien.com.usmalumniapp.Manifest;
import usmalumni.emerien.com.usmalumniapp.model.Achievements;
import usmalumni.emerien.com.usmalumniapp.model.Alumni;

/**
 * Created by Philip on 02/17/2018.
 */

public class ProfileRetrievePresenter implements ProfileView.RetrieveProfile {

    public final static String TAG = ProfileRetrievePresenter.class.getSimpleName();

    RetrieveProfileCallback cb;
    RetrieveAchivementsCallback achivementsCallback;
    RetrieveAchievementsCallbackMethod2 achievementsCallbackMethod2;
    DatabaseReference ref;
    DatabaseReference accomplishmentsRef;
    String uuid;


    public ProfileRetrievePresenter(RetrieveProfileCallback callback) {
        this.cb=callback;
    }

    public ProfileRetrievePresenter(String uuid) {

        this.uuid=uuid;
    }

    @Override
    public void getUserProfile(String userUUid) {
        ref= FirebaseDatabase.getInstance().getReference().child("Alumni").child(userUUid);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Alumni alumni =dataSnapshot.getValue(Alumni.class);
                cb.successRetrievingProfile(alumni);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                cb.errorRetrievingProfile(databaseError);
            }
        });

    }


    @Override
    public void getUserAchievements() {

        accomplishmentsRef= FirebaseDatabase.getInstance().getReference().child("Achievements");
        accomplishmentsRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Achievements achievements= dataSnapshot.getValue(Achievements.class);
                Log.e(TAG, "onChildAdded: "+achievements.getAchievementDetails() );
                if (achievements.getUserUUID().equals(uuid)) {
                    Log.e(TAG, "onChildAdded: "+achievements.getUserUUID()+"\t"+uuid );
                    achivementsCallback.onChildAdded(achievements);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Achievements achievements= dataSnapshot.getValue(Achievements.class);
                if (achievements.getUserUUID().equals(uuid)) {
                    achivementsCallback.onChildChanged(achievements,s);
                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Achievements achievements= dataSnapshot.getValue(Achievements.class);
                if (achievements.getUserUUID().equals(uuid)) {
                    achivementsCallback.onChildRemoved(achievements);
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Achievements achievements= dataSnapshot.getValue(Achievements.class);
                if (achievements.getUserUUID().equals(uuid)) {
                    achivementsCallback.onChildMoved(achievements,s);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                achivementsCallback.onCancelled(databaseError);
            }
        });

    }

    @Override
    public void getUserAchievementsAnotherMethod(final String userUUID) {
        accomplishmentsRef= FirebaseDatabase.getInstance().getReference().child("Achievements");
        accomplishmentsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                List<Achievements> achievementsList= dataSnapshot.getValue(List.class);
                List<Achievements> filteredAchievements= new ArrayList<>();

              /*  for (int i=0; i< achievementsList.size();i++) {
                    if (achievementsList.get(i).getUserUUID().equals(uuid)) {
                        filteredAchievements.add(achievementsList.get(i));
                    }
                }
                achievementsCallbackMethod2.onDataChanged(filteredAchievements);*/

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Achievements achievements= ds.getValue(Achievements.class);
                    Log.e(TAG, "onDataChange: USER UUID: "+achievements.getUserUUID()+"\t"+userUUID);
                    if (achievements.getUserUUID().equals(userUUID)) {
                        filteredAchievements.add(achievements);
                    }
                }

                achievementsCallbackMethod2.onDataChanged(filteredAchievements);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    public void setRetrieveAchievementMethod1Listener(RetrieveAchivementsCallback cb) {
        this.achivementsCallback=cb;
    }

    public void setRetrieveAchievementsMethod2Listener(RetrieveAchievementsCallbackMethod2 cb) {
        this.achievementsCallbackMethod2=cb;
    }



    public interface RetrieveProfileCallback {
        void successRetrievingProfile(Alumni alumni);
        void errorRetrievingProfile(DatabaseError err);
    }

    public interface RetrieveAchivementsCallback {
        void onChildAdded(Achievements achievements);
        void onChildChanged(Achievements achievements, String s);
        void onChildRemoved(Achievements achievements);
        void onChildMoved(Achievements achievements,String s);
        void onCancelled(DatabaseError databaseError);
    }
    public interface RetrieveAchievementsCallbackMethod2{
        void onDataChanged(List<Achievements> achievementsList);
    }
}

package usmalumni.emerien.com.usmalumniapp.helpers;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

/**
 * Created by Philip on 03/19/2018.
 */

public class InternetConnectionBroadcastReciever extends BroadcastReceiver {

    public final static String TAG = InternetConnectionBroadcastReciever.class.getSimpleName();


    @Override
    public void onReceive(Context context, Intent intent) {

        Log.e(TAG, "onReceive: INTERNET CONNECTIVITY CHANGES!" );
        if (!AppUtils.isDeviceConnectedToAnyNetworks(context)) {
//            redirectToWifi(context);
        }
    }
    private void redirectToWifi(Context ctx) {
        final Context refCtx=ctx;
        AlertDialog.Builder builder= new AlertDialog.Builder(ctx)
                .setMessage("No internet connection. Please connect to wifi or mobile data")
                .setPositiveButton("Wifi Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        refCtx.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                });

        AlertDialog dialog= builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.show();
    }



}

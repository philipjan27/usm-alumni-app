package usmalumni.emerien.com.usmalumniapp.authentication;

import android.app.Activity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by Philip on 01/06/2018.
 */

public interface AuthView {

    interface AuthMethod {
        void checkLogin(Activity act, FirebaseAuth auth, String email, String password);
    }

    interface AuthCheckOnStart {
        void checkIfAlreadyLoggedIn(FirebaseUser user, FirebaseAuth auth);
    }
    void onSuccess(FirebaseAuth auth);
    void onError();
    void onFailure();

    void onAlreadyLoggedIn(FirebaseUser user);
    void onNotAlreadyLogon();
}

package usmalumni.emerien.com.usmalumniapp.model;

/**
 * Created by Philip on 02/25/2018.
 */

public class Achievements {
    private String uuid;
    private String achievementDetails;
    private String userUUID;

    public Achievements() {
    }

    public Achievements(String uuid, String achievementDetails, String userUUID) {
        this.uuid = uuid;
        this.achievementDetails = achievementDetails;
        this.userUUID = userUUID;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAchievementDetails() {
        return achievementDetails;
    }

    public void setAchievementDetails(String achievementDetails) {
        this.achievementDetails = achievementDetails;
    }

    public String getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(String userUUID) {
        this.userUUID = userUUID;
    }
}

package usmalumni.emerien.com.usmalumniapp.editprofile;

import usmalumni.emerien.com.usmalumniapp.model.Alumni;

/**
 * Created by Philip on 03/06/2018.
 */

public interface EditProfileView {

    public interface saveEditedProfile{
        void saveNewEditedProfile(Alumni toBeEdited, Alumni newProfileUpdated);
    }
}

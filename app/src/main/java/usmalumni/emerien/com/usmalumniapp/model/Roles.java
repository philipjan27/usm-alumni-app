package usmalumni.emerien.com.usmalumniapp.model;

/**
 * Created by Philip on 01/04/2018.
 */

public class Roles {

    private String uuid;
    private String role;

    public Roles( String role) {
        this.role = role;
    }

    public Roles(String uuid, String role) {
        this.uuid = uuid;
        this.role = role;
    }

    public Roles() {
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}

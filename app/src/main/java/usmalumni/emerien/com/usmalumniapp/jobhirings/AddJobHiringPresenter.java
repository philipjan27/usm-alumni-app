package usmalumni.emerien.com.usmalumniapp.jobhirings;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import usmalumni.emerien.com.usmalumniapp.model.Jobs;

/**
 * Created by Philip on 01/23/2018.
 */

public class AddJobHiringPresenter implements JobHiringView.AddNewJobHiring {

    JobHiringView v;

    public AddJobHiringPresenter(JobHiringView v) {
        this.v=v;
    }

    @Override
    public void addNewJobHiring(Jobs jobs) {
        DatabaseReference ref= FirebaseDatabase.getInstance().getReference().child("Jobs");
        jobs.setUuid(ref.push().getKey());
        ref.child(jobs.getUuid()).setValue(jobs).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                v.addJobSuccess();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                v.addJobFailure();
            }
        });
    }
}

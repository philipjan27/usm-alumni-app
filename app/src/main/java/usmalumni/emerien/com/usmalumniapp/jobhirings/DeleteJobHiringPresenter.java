package usmalumni.emerien.com.usmalumniapp.jobhirings;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Philip on 02/13/2018.
 */

public class DeleteJobHiringPresenter implements JobHiringView.DeleteJobHiring {

    public final static String TAG = DeleteJobHiringPresenter.class.getSimpleName();

    DatabaseReference ref;
    DeleteInterfaceCallback cb;

    public DeleteJobHiringPresenter(DeleteInterfaceCallback cb){
        this.cb=cb;
    }

    @Override
    public void deleteJobHiring(final String uuid) {
        Thread t= new Thread(new Runnable() {
            @Override
            public void run() {
                ref= FirebaseDatabase.getInstance().getReference().child("Jobs");
                ref.child(uuid).removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                cb.jobHiringDeleteSuccess();
                                Log.e(TAG, "onSuccess: DELETE JOB HIRING");
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                             cb.jobHiringDeleteFailed();
                                Log.e(TAG, "onFailure: DELETE JOB HIRING");
                            }
                        });
            }
        });
        t.start();
    }

    public interface DeleteInterfaceCallback {
        void jobHiringDeleteSuccess();
        void jobHiringDeleteFailed();
    }
}

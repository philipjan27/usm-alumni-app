package usmalumni.emerien.com.usmalumniapp.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.auth.FirebaseUser;


/**
 * Created by Philip on 01/06/2018.
 */

public class SharedPreferenceHelper {

    public final static String TAG = SharedPreferenceHelper.class.getSimpleName();

    private static SharedPreferenceHelper instance=null;
    static SharedPreferences prefs;

    public static SharedPreferenceHelper getInstance() {
      if (instance == null) {
          instance= new SharedPreferenceHelper();
      }
      return instance;
    }

    public SharedPreferences initializePrefs(Context ctx) {
        SharedPreferences prefs= ctx.getSharedPreferences("usmalumni.emerien.com.usmalumniapp",Context.MODE_PRIVATE);
        return prefs;
    }

    public void setPrefsContent(SharedPreferences prefs, FirebaseUser user) {
        SharedPreferences.Editor editor= prefs.edit();
        editor.putBoolean("loggedin",true);
        editor.putString("email",user.getEmail());
        editor.putString("uid",user.getUid());
        editor.commit();

        Log.e(TAG, "setPrefsContent: \n"+"loggedin -> "+true+"\n"+"name -> "+user.getDisplayName()+"\n"+"email -> "+user.getEmail()+"\n"+"uid -> "+user.getUid()+"\n");
    }

    public boolean checkIfLoggedIn(SharedPreferences prefs) {

        if (prefs.getBoolean("loggedin",false) == true) {
            Log.e(TAG, "checkIfLoggedIn: True");
            return true;

        }
        Log.e(TAG, "checkIfLoggedIn: False");
        return false;
    }

    public void cleanPrefs(SharedPreferences pref) {
        SharedPreferences.Editor editor= pref.edit();
        editor.clear();
        editor.commit();
    }

    public void saveCurrentRoleToPrefs(SharedPreferences prefs,String role) {
        SharedPreferences.Editor editor= prefs.edit();
        editor.putString("role",role);
        editor.commit();
    }

    public String getCurrentRoleInPrefs(SharedPreferences prefs) {
       String role= prefs.getString("role","");
        return role;
    }

    public void saveUserUUIDtoPrefs(SharedPreferences prefs,String uuid) {
        SharedPreferences.Editor editor= prefs.edit();
        editor.putString("uuid",uuid);
        editor.commit();
    }

    public String getUserUUIDInPrefs(SharedPreferences prefs){
        String userUUID= prefs.getString("uuid","");
        return userUUID;
    }
    public String getCurrentUserEmail(SharedPreferences prefs){
        String email=prefs.getString("email","");
        return email;
    }
}

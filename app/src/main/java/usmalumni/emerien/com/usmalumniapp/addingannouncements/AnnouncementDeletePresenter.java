package usmalumni.emerien.com.usmalumniapp.addingannouncements;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * Created by Philip on 01/28/2018.
 */

public class AnnouncementDeletePresenter implements AnnouncementsView.DeleteAnnouncement {

    public final static String TAG = AnnouncementDeletePresenter.class.getSimpleName();

    StorageReference storageRef,imageStorageRef;
    DatabaseReference announcementRef;
    DeleteInterfaceCallback v;

    public AnnouncementDeletePresenter(DeleteInterfaceCallback v) {
        this.v = v;

    }

    @Override
    public void deleteAnnouncement(final String uuid) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

                announcementRef = FirebaseDatabase.getInstance().getReference().child("Announcements");
                announcementRef.child(uuid).removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.e(TAG, "onSuccess: DELETED " + uuid);
                                v.announcementDeletionSuccess();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "onFailure: ERROR IN DELETION: " + e.getMessage());
                        v.announcementDeletionError();
                    }
                });

            }
        });
        t.start();
    }

    @Override
    public void deleteImageFile(final String imageUrl) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                storageRef= FirebaseStorage.getInstance().getReference();
                imageStorageRef = storageRef.child("announcements/"+imageUrl);
                imageStorageRef.delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.e(TAG, "onSuccess: SUCCESS DELETING FILE" );
                                v.announcementImageDeleteSuccess();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.e(TAG, "onFailure: FAILED TO DELETE FILE ERROR: "+e.getMessage() );
                                v.announcementImageDeleteFailure();
                            }
                        });

            }
        });
        t.start();
    }

    public interface DeleteInterfaceCallback {

        void announcementDeletionSuccess();
        void announcementDeletionError();

        void announcementImageDeleteSuccess();
        void announcementImageDeleteFailure();
    }
}

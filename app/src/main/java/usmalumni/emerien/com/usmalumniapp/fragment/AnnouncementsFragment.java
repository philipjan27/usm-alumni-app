package usmalumni.emerien.com.usmalumniapp.fragment;


import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.BasePermissionListener;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import usmalumni.emerien.com.usmalumniapp.R;
import usmalumni.emerien.com.usmalumniapp.adapter.AnnouncementsRecyclerviewAdapter;
import usmalumni.emerien.com.usmalumniapp.addingannouncements.AnnouncementDeletePresenter;
import usmalumni.emerien.com.usmalumniapp.addingannouncements.AnnouncementEditPresenter;
import usmalumni.emerien.com.usmalumniapp.addingannouncements.AnnouncementsAddPresenter;
import usmalumni.emerien.com.usmalumniapp.addingannouncements.AnnouncementsView;
import usmalumni.emerien.com.usmalumniapp.helpers.AppUtils;
import usmalumni.emerien.com.usmalumniapp.helpers.RealPathUtil;
import usmalumni.emerien.com.usmalumniapp.helpers.SharedPreferenceHelper;
import usmalumni.emerien.com.usmalumniapp.model.Announcements;


public class AnnouncementsFragment extends Fragment implements AnnouncementsView, AnnouncementsRecyclerviewAdapter.ItemClickedCallback, AnnouncementEditPresenter.AnnouncementEditCallback {

    public final static String TAG = AnnouncementsFragment.class.getSimpleName();

    List<Announcements> announcementsList;
    AnnouncementEditPresenter editPresenter;
    AnnouncementsRecyclerviewAdapter adapter;
    AnnouncementsAddPresenter addPresenter;
    SharedPreferenceHelper helper;
    SharedPreferences prefs;
    Uri imageUri = null;
    Announcements announcementsToBeEditedFromCallback = null;

    ProgressDialog progressDialog;
    View v;
    FloatingActionButton fab;
    RecyclerView rv;


    //  WIDGETS FROM THE ALERT DIALOG
    EditText announcement;
    Button imageUpload;


    @Override
    public void onStart() {
        super.onStart();
        initializePermissions();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPresenter = new AnnouncementsAddPresenter(this, getActivity());
        editPresenter = new AnnouncementEditPresenter(this, getActivity());
        helper = SharedPreferenceHelper.getInstance();
        prefs = helper.initializePrefs(getActivity());

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 90 && resultCode == getActivity().RESULT_OK && data != null && data.getData() != null) {
            if (data != null) {
                imageUri = data.getData();
                imageUpload.setText(RealPathUtil.getRealPathFromURI_API19(getActivity(), imageUri));
                Toasty.success(getActivity(), "Image selected successfully.", Toast.LENGTH_LONG, true).show();
                Log.i(TAG, "onActivityResult: 90 ---> " + imageUri);
            } else {
                Log.e(TAG, "onActivityResult: Null image URI!");
                Toasty.error(getActivity(), "Failed to choose image to upload.", Toast.LENGTH_LONG, true).show();
            }

        } else if (requestCode == 100 && resultCode == getActivity().RESULT_OK && data != null && data.getData() != null) {
            if (data != null) {
                imageUri = data.getData();
                imageUpload.setText(RealPathUtil.getRealPathFromURI_API19(getActivity(), imageUri));
                Toasty.success(getActivity(), "Image selected successfully.", Toast.LENGTH_LONG, true).show();
                Log.e(TAG, "onActivityResult: 100 ---> " + imageUri);
            } else {
                Log.e(TAG, "onActivityResult: Null image URI!");
                Toasty.error(getActivity(), "Failed to choose image to upload.", Toast.LENGTH_LONG, true).show();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_announcements, container, false);
        fab = v.findViewById(R.id.announcements_fab);
        rv = v.findViewById(R.id.announcements_recyclerview);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Announcements");
        progressDialog.setMessage("Publishing announcements to server..");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        llm.setReverseLayout(true);
        llm.setStackFromEnd(true);

        rv.setLayoutManager(llm);


        return v;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNewAnnouncements("Add Announcement", 90);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        announcementsList = new ArrayList<>();

        if (!helper.getCurrentRoleInPrefs(prefs).equals("Admin")) {
            Log.e(TAG, "onResume: CURRENT ROLE: "+helper.getCurrentRoleInPrefs(prefs) );
            fab.setVisibility(View.GONE);
            adapter = new AnnouncementsRecyclerviewAdapter(false, this);
        } else {
            fab.setVisibility(View.VISIBLE);
            adapter = new AnnouncementsRecyclerviewAdapter(true, this);
        }

        rv.setAdapter(adapter);
        Log.e(TAG, "onResume: ");

    }


    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPause: ");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy:");
    }

    /**
     * Adding/Editing announcement
     *
     * @param titleText
     * @param requestCode
     */
    private void addNewAnnouncements(String titleText, final int requestCode) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_DARK);

        View v = getActivity().getLayoutInflater().inflate(R.layout.custom_add_announcementsforms_layout, null);
        imageUpload = (Button) v.findViewById(R.id.imageupload_announcements);
        announcement = (EditText) v.findViewById(R.id.announcements_editext);

        /**
         *      REQUEST CODE=100 --> EDITING ANNOUNCEMENT
         *      REQUEST CODE=90 --> ADDING NEW ANNOUNCEMENT
         */
        if (requestCode == 100) {
            imageUpload.setText(announcementsToBeEditedFromCallback.getLocalImageFileName());
            announcement.setText(announcementsToBeEditedFromCallback.getDescription());
        }

        builder.setView(v);
        builder.setTitle(titleText);
        builder.setInverseBackgroundForced(true);
        builder.setPositiveButton("PUBLISH", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (AppUtils.isDeviceConnectedToAnyNetworks(getActivity())) {

                    if (requestCode == 90) {


                        if (imageUri == null) {
                            imageUri = Uri.parse("android.resource://usmalumni.emerien.com.usmalumniapp/drawable/ic_picture_in_picture_black_48dp");
                        }

                        progressDialog.show();
                        Announcements announcements = new Announcements();
                        announcements.setDescription(announcement.getText().toString());
                        announcements.setPublishDate(System.currentTimeMillis());
                        addPresenter.uploadImageToFbStorage(announcements, imageUri);

                    } else if (requestCode == 100) {

                        if (imageUri == null || imageUri.equals("")) {

                            progressDialog.show();
                            Announcements newAnnouncements = new Announcements();
                            newAnnouncements.setDescription(announcement.getText().toString());
                            newAnnouncements.setUuid(announcementsToBeEditedFromCallback.getUuid());
                            newAnnouncements.setImageFile(announcementsToBeEditedFromCallback.getImageFile());
                            newAnnouncements.setLocalImageFileName(announcementsToBeEditedFromCallback.getLocalImageFileName());
                            newAnnouncements.setPublishDate(System.currentTimeMillis());
                            editPresenter.editAnnouncement(announcementsToBeEditedFromCallback, newAnnouncements, imageUri);

                        } else {

                            progressDialog.show();
                            Announcements newAnnouncements = new Announcements();
                            newAnnouncements.setDescription(announcement.getText().toString());
                            newAnnouncements.setUuid(announcementsToBeEditedFromCallback.getUuid());
                            newAnnouncements.setLocalImageFileName(announcementsToBeEditedFromCallback.getLocalImageFileName());
                            newAnnouncements.setPublishDate(System.currentTimeMillis());
                            editPresenter.editAnnouncement(announcementsToBeEditedFromCallback, newAnnouncements, imageUri);

                        }

                    }
                }else {
                    Toasty.error(getActivity(),"No internet connection. Please try again",Toast.LENGTH_SHORT,true).show();
                }


            }
        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });


        imageUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), requestCode);
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();


    }


    /**
     * CALLABACK FOR ADDING NEW ANNOUNCEMENTS
     */

    @Override
    public void addAnnouncementSuccess() {
        progressDialog.dismiss();
        Toasty.success(getActivity(), "Successfull adding new announcement", Toast.LENGTH_LONG, true).show();
        Log.e(TAG, "addAnnouncementSuccess: Adding announcements success!");
    }

    @Override
    public void errorAnnoucementPublish() {
        progressDialog.dismiss();
        Toasty.error(getActivity(), "Failed to publish announcements, please try again", Toast.LENGTH_LONG, true).show();
        Log.e(TAG, "errorAnnoucementPublish: Error adding announcement!");
    }

    @Override
    public void imageUploadError(Exception e) {
        progressDialog.dismiss();
        Toasty.error(getActivity(), "Failed to upload image \n" + e.getMessage(), Toast.LENGTH_LONG, true).show();
    }

    @Override
    public void imageUploadSuccess() {

    }


    /**
     * WHEN EDIT / DELETE CLICKED INSIDE RECYCLREVIEW
     *
     * @param announcements
     * @param itemUUId
     */

    @Override
    public void editClicked(Announcements announcements, String itemUUId) {
        announcementsToBeEditedFromCallback = announcements;
        addNewAnnouncements("Edit Announcement", 100);
    }

    @Override
    public void deleteClicked(int position, String itemUUID) {
    }


    /**
     * ANNOUNCEMENT EDIT CALLBACK LISTENER!
     */

    @Override
    public void onSuccessEditAnnouncement() {
        progressDialog.dismiss();
        Toasty.success(getActivity(), "Successfully edited announcement", Toast.LENGTH_LONG, true).show();
    }

    @Override
    public void onFailureEditAnnouncement() {
        progressDialog.dismiss();
        Toasty.error(getActivity(), "An error occured when editing data", Toast.LENGTH_LONG, true).show();
    }

    @Override
    public void imageReuploadSuccess() {
        // if image upload is success
    }

    @Override
    public void imageReuploadFailed() {
        // if image reupload failed
    }

    private void initializePermissions() {
       /* Dexter.withActivity(getActivity()).withPermission(Manifest.permission.READ_EXTERNAL_STORAGE).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                Log.i(TAG, "onPermissionGranted: SUCCESS ACQUIRING PERMISSION");
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                Log.e(TAG, "onPermissionDenied: PERMISSION DENIED");
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();*/

       Dexter.withActivity(getActivity()).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_NETWORK_STATE,Manifest.permission.INTERNET)
               .withListener(new MultiplePermissionsListener() {
                   @Override
                   public void onPermissionsChecked(MultiplePermissionsReport report) {
                       Log.e(TAG, "onPermissionsChecked: ALL PERMISSION GRANTED: "+report.areAllPermissionsGranted());
                   }

                   @Override
                   public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                       Log.e(TAG, "onPermissionRationaleShouldBeShown: " );
                       token.continuePermissionRequest();
                   }
               }).check();
    }
}

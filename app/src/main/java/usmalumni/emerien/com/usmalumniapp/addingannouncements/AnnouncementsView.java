package usmalumni.emerien.com.usmalumniapp.addingannouncements;

import android.net.Uri;

import java.util.List;

import usmalumni.emerien.com.usmalumniapp.adapter.AnnouncementsRecyclerviewAdapter;
import usmalumni.emerien.com.usmalumniapp.model.Announcements;

/**
 * Created by Philip on 01/21/2018.
 */

public interface AnnouncementsView {

    interface AddAnnouncements {
        void uploadImageToFbStorage(Announcements announcements, Uri imageUri);
    }

    interface RetrieveAnnouncements {
        void getListOfAnnouncements();
    }

    interface EditAnnouncement {
        void editAnnouncement(Announcements oldAnnouncements,Announcements newAnnouncements,Uri imageUri);
    }

    interface DeleteAnnouncement {
        void deleteAnnouncement(String uuid);
        void deleteImageFile(String imageUrl);
    }

    void addAnnouncementSuccess();
    void errorAnnoucementPublish();

    void imageUploadError(Exception e);
    void imageUploadSuccess();



}

package usmalumni.emerien.com.usmalumniapp.adapter;

import android.app.AlarmManager;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import usmalumni.emerien.com.usmalumniapp.R;
import usmalumni.emerien.com.usmalumniapp.model.Alumni;

/**
 * Created by Philip on 03/14/2018.
 */

public class SearchUsersAdapter extends RecyclerView.Adapter<SearchUsersAdapter.ViewHolder> implements Filterable {

    public final static String TAG = SearchUsersAdapter.class.getSimpleName();

    List<Alumni> alumniList;
    List<Alumni> filteredList;
    OnAlumniSelectedListener cb;
    View v;
    UserFilter userFilter;


    public SearchUsersAdapter(List<Alumni> alumniList) {
        this.alumniList = alumniList;
        filteredList = alumniList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        v = inflater.inflate(R.layout.searchuser_recyclerlayout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (filteredList.get(position).getImageFile().isEmpty() || filteredList.get(position).getImageFile() == null || filteredList.get(position).getImageFile().trim().isEmpty()) {
            Picasso.with(v.getContext()).load(R.drawable.ic_emptyprofilepic).centerCrop().resize(70,70).into(holder.profImage);
        } else {
            Picasso.with(v.getContext()).load(filteredList.get(position).getImageFile()).centerCrop().resize(70,70).into(holder.profImage);
        }

        if (filteredList.get(position).getEmail().isEmpty() && filteredList.get(position).getFullName().isEmpty()) {
            holder.email.setText("EMPTY VALUES");
            holder.fullName.setText("EMPTY VALUES");
        } else {
            holder.email.setText(filteredList.get(position).getEmail());
            holder.fullName.setText(filteredList.get(position).getFullName());
        }
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    @Override
    public Filter getFilter() {
        if (userFilter == null) {
            userFilter = new UserFilter();
        }

        return userFilter;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CircleImageView profImage;
        TextView fullName, email;

        public ViewHolder(View itemView) {
            super(itemView);
            profImage = (CircleImageView) itemView.findViewById(R.id.userImage);
            fullName = (TextView) itemView.findViewById(R.id.userFullName);
            email = (TextView) itemView.findViewById(R.id.userEmail);

            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            Log.e(TAG, "onClick: "+filteredList.get(getAdapterPosition()).getFullName() );
            int pos = getAdapterPosition();
            cb.onAlumniSelected(filteredList.get(pos));
        }
    }

    public void setAlumniSelectedListener(OnAlumniSelectedListener listener) {
        this.cb = listener;
    }

    public interface OnAlumniSelectedListener {
        void onAlumniSelected(Alumni alumni);
    }

    private class UserFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            String currword= charSequence.toString().toLowerCase(Locale.getDefault());
            FilterResults filterResults = new FilterResults();
            List<Alumni> alumniListFiltered = null;
            if (charSequence != null && charSequence.length() > 0) {
                alumniListFiltered = new ArrayList<>();
                for (Alumni alumni : alumniList) {
                    if (alumni.getFullName().toLowerCase(Locale.getDefault()).contains(currword)) {
                        alumniListFiltered.add(alumni);
                        Log.e(TAG, "performFiltering: " + alumni.getFullName() + " ---> " + charSequence);
                    }
                }

                Log.e(TAG, "performFiltering: Character>0 SIZE: " + alumniListFiltered.size());
                filterResults.values = alumniListFiltered;
                filterResults.count = alumniListFiltered.size();
                return filterResults;

            } else {
                Log.e(TAG, "performFiltering: Character==0 SIZE: " + alumniList.size());
                filterResults.values = alumniList;
                filterResults.count = alumniList.size();
                return filterResults;
            }


//            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            try {
                filteredList = (List<Alumni>) filterResults.values;
                notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "publishResults: Excemption detected! " + e.getMessage());
            }
        }

    }

}

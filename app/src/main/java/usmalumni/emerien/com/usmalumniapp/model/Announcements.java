package usmalumni.emerien.com.usmalumniapp.model;

import java.util.List;

/**
 * Created by Philip on 01/04/2018.
 */

public class Announcements {

    private String uuid;
    private String description;
    private long publishDate;
    private String imageFile;
    private String localImageFileName;

    /*public Announcements(String uuid, String description, long publishDate, String imageFile) {
        this.uuid = uuid;
        this.description = description;
        this.publishDate = publishDate;
        this.imageFile = imageFile;
    }*/

    public Announcements(String uuid, String description, long publishDate, String imageFile, String localImageFileName) {
        this.uuid = uuid;
        this.description = description;
        this.publishDate = publishDate;
        this.imageFile = imageFile;
        this.localImageFileName = localImageFileName;
    }

    public Announcements() {
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(long publishDate) {
        this.publishDate = publishDate;
    }

    public String getImageFile() {
        return imageFile;
    }

    public void setImageFile(String imageFile) {
        this.imageFile = imageFile;
    }

    public String getLocalImageFileName() {
        return localImageFileName;
    }

    public void setLocalImageFileName(String localImageFileName) {
        this.localImageFileName = localImageFileName;
    }
}

package usmalumni.emerien.com.usmalumniapp.dashboard;

import usmalumni.emerien.com.usmalumniapp.model.Alumni;

/**
 * Created by Philip on 01/15/2018.
 */

public interface DashboardView {

    interface ProfileDetails {
        void getProfileDetails(String uuid);
    }

    void retrieveRoleProfileDetails(Alumni alumni);

}

package usmalumni.emerien.com.usmalumniapp.addingannouncements;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import usmalumni.emerien.com.usmalumniapp.adapter.AnnouncementsRecyclerviewAdapter;
import usmalumni.emerien.com.usmalumniapp.model.Announcements;

/**
 * Created by Philip on 01/21/2018.
 */

public class AnnouncementsRetrievePresenter implements AnnouncementsView.RetrieveAnnouncements {

    RetrieveAnnouncementListCallback retrieveAnnouncementListCallback;
   retrieveAnnouncementsList cb;

   public void setRetrieveAnnouncementListCallback(RetrieveAnnouncementListCallback cb){
       this.retrieveAnnouncementListCallback=cb;
   }

   /* public AnnouncementsRetrievePresenter (retrieveAnnouncementsList callback) {
        this.cb=callback;
    }*/

  /*  @Override
    public void getListOfAnnouncements() {


        DatabaseReference ref= FirebaseDatabase.getInstance().getReference().child("Announcements");
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                cb.onChildAdded(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                cb.onChildChanged(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                cb.onChildRemoved(dataSnapshot);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                cb.onChildMoved(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                cb.onCancelled(databaseError);
            }
        });

    }*/

    @Override
    public void getListOfAnnouncements() {


        DatabaseReference ref= FirebaseDatabase.getInstance().getReference().child("Announcements");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Announcements> announcementsList= new ArrayList<>();

                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Announcements announcementsObj= ds.getValue(Announcements.class);
                    announcementsList.add(announcementsObj);
                }

                retrieveAnnouncementListCallback.getAnnouncementList(announcementsList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    public interface RetrieveAnnouncementListCallback{
        void getAnnouncementList(List<Announcements> announcements);
    }
    public interface retrieveAnnouncementsList {
        void onChildAdded(DataSnapshot ds);
        void onChildChanged(DataSnapshot ds);
        void onChildRemoved(DataSnapshot ds);
        void onChildMoved(DataSnapshot ds);
        void onCancelled(DatabaseError de);
    }

}

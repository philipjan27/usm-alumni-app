package usmalumni.emerien.com.usmalumniapp.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.google.firebase.database.DatabaseError;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import usmalumni.emerien.com.usmalumniapp.R;
import usmalumni.emerien.com.usmalumniapp.adapter.AchievementsProfileAdapter;
import usmalumni.emerien.com.usmalumniapp.model.Achievements;
import usmalumni.emerien.com.usmalumniapp.model.Alumni;
import usmalumni.emerien.com.usmalumniapp.ownprofile.AchievementsTransactionPresenter;
import usmalumni.emerien.com.usmalumniapp.ownprofile.ProfileRetrievePresenter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewSelectedFragment extends Fragment implements ProfileRetrievePresenter.RetrieveProfileCallback, ProfileRetrievePresenter.RetrieveAchievementsCallbackMethod2 {

    View v;
    CircleImageView circleImageView;
    FloatingActionButton editProfile;
    TextView fullname, email, mobileNumber, course, address, batch, civilStatus, jobPosition, company;
    ExpandableHeightListView accomplishments;

    boolean alreadyRetrievedProfile, alreadyRetrievedAchievement=false;
    String uuid;
    Bundle bundle;
    ProfileRetrievePresenter retrievePresenter;

    /**
     * ACHIEVEMENTS ADAPTER, PRESENTER STUFFS
     */
    AchievementsProfileAdapter achievementsProfileAdapter;
    List<Achievements> achievementsListRef = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();

        if (bundle != null) {
            uuid = bundle.getString("uuid");
        }

        retrievePresenter = new ProfileRetrievePresenter(this);
        retrievePresenter.getUserProfile(uuid);
        retrievePresenter.getUserAchievementsAnotherMethod(uuid);
        retrievePresenter.setRetrieveAchievementsMethod2Listener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_profile, container, false);
        achievementsProfileAdapter = new AchievementsProfileAdapter(getActivity(), uuid, achievementsListRef, true);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fullname = (TextView) v.findViewById(R.id.profile_fullname);
        email = (TextView) v.findViewById(R.id.profile_emailadd);
        mobileNumber = (TextView) v.findViewById(R.id.profile_mobilenumber);
        course = (TextView) v.findViewById(R.id.profile_course);
        address = (TextView) v.findViewById(R.id.profile_address);
        batch = (TextView) v.findViewById(R.id.profile_batch);
        civilStatus = (TextView) v.findViewById(R.id.profile_civilstatus);
        jobPosition = (TextView) v.findViewById(R.id.profile_jobPosition);
        company = (TextView) v.findViewById(R.id.profile_company);
        editProfile = (FloatingActionButton) v.findViewById(R.id.profile_edit);
        circleImageView = (CircleImageView) v.findViewById(R.id.profile_circularimageview);
        accomplishments = (ExpandableHeightListView) v.findViewById(R.id.profile_expandableheightLv);
        accomplishments.setExpanded(true);
        editProfile.setVisibility(View.GONE);


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * RETRIEVE USER PROFILE
     *
     * @param alumni
     */
    @Override
    public void successRetrievingProfile(Alumni alumni) {
        fullname.setText(alumni.getFullName());
        email.setText(alumni.getEmail());
        mobileNumber.setText(alumni.getMobileNumber());
        course.setText(alumni.getCourse());
        address.setText(alumni.getAddress());
        batch.setText(alumni.getYearBatch());
        civilStatus.setText(alumni.getCivilStatus());
        jobPosition.setText(alumni.getJobPosition());
        company.setText(alumni.getCompanyName());

        /*if (!alumni.getImageFile().isEmpty() || alumni.getImageFile() != "") {
            if (!alumni.getImageFile().equals(" ")){
                Picasso.with(getActivity()).load(R.drawable.ic_emptyprofilepic).into(circleImageView);
            }else {
                Picasso.with(getActivity()).load(alumni.getImageFile()).into(circleImageView);
            }
        }else {
            Picasso.with(getActivity()).load(R.drawable.ic_emptyprofilepic).into(circleImageView);
        }*/

        if (alumni.getImageFile().isEmpty() || alumni.getImageFile().equals("")) {
//            Picasso.with(getActivity()).load(R.drawable.ic_emptyprofilepic).resize(70, 70).onlyScaleDown().into(circleImageView);
            Picasso.with(getActivity()).load(R.drawable.ic_emptyprofilepic).centerCrop().resize(100,100).into(circleImageView);
        } else {
//            Picasso.with(getActivity()).load(alumni.getImageFile()).resize(70, 70).onlyScaleDown().into(circleImageView);
            Picasso.with(getActivity()).load(alumni.getImageFile()).centerCrop().resize(100,100).into(circleImageView);
        }
    }

    @Override
    public void errorRetrievingProfile(DatabaseError err) {
        Toasty.error(getActivity(),"Error in retrieving profile, please check your internet connection", Toast.LENGTH_LONG,true).show();
    }


    /**
     * RETRIEVE ACHIEVEMENTS
     * @param achievementsList
     */
    @Override
    public void onDataChanged(List<Achievements> achievementsList) {
        if (!achievementsListRef.isEmpty()) {
            achievementsListRef.clear();
        }
        achievementsListRef = achievementsList;
        updateAchievementList(achievementsListRef);

    }

    private void updateAchievementList(List<Achievements> achievementsList) {
        achievementsProfileAdapter = new AchievementsProfileAdapter(getActivity(), uuid, achievementsList, true);
        accomplishments.setAdapter(achievementsProfileAdapter);
        achievementsProfileAdapter.notifyDataSetChanged();
    }

}

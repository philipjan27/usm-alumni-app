package usmalumni.emerien.com.usmalumniapp.jobhirings;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import usmalumni.emerien.com.usmalumniapp.model.Jobs;

/**
 * Created by Philip on 01/23/2018.
 */

public class RetrieveJobHiringsPresenter implements JobHiringView.RetrieveJobsList {

    GetListOfJobsList callback;
    GetListOfJobsListCallback getListOfJobsListCallback;

   /* public RetrieveJobHiringsPresenter(GetListOfJobsList cb){
        this.callback=cb;
    }*/
   public void setGetListOfJobsListCallback(GetListOfJobsListCallback cb){
       this.getListOfJobsListCallback=cb;
   }


  /*  @Override
    public void getJobsList() {
        DatabaseReference reference= FirebaseDatabase.getInstance().getReference().child("Jobs");
        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                callback.onChildAdded(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                callback.onChildChanged(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                callback.onChildRemoved(dataSnapshot);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                callback.onChildMoved(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onCancelled(databaseError);
            }
        });

    }*/

    @Override
    public void getJobsList() {
        DatabaseReference reference= FirebaseDatabase.getInstance().getReference().child("Jobs");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Jobs> jobsList= new ArrayList<>();
                for (DataSnapshot ds: dataSnapshot.getChildren()){
                    Jobs jobsObj= ds.getValue(Jobs.class);
                    jobsList.add(jobsObj);
                }

                getListOfJobsListCallback.getListOfJobsList(jobsList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public interface GetListOfJobsListCallback{
        void getListOfJobsList(List<Jobs> jobsList);
    }

    public interface GetListOfJobsList {
        void onChildAdded(DataSnapshot ds);
        void onChildChanged(DataSnapshot ds);
        void onChildRemoved(DataSnapshot ds);
        void onChildMoved(DataSnapshot ds);
        void onCancelled(DatabaseError err);
    }
}

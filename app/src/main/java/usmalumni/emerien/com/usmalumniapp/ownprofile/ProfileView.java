package usmalumni.emerien.com.usmalumniapp.ownprofile;

import usmalumni.emerien.com.usmalumniapp.model.Achievements;
import usmalumni.emerien.com.usmalumniapp.model.Alumni;

/**
 * Created by Philip on 02/17/2018.
 */

public interface ProfileView {

    public interface RetrieveProfile {
        void getUserProfile(String userUUid);

        void getUserAchievements();

        void getUserAchievementsAnotherMethod(String userUUID);
    }

    public interface EditProfile {
        void editUserProfile(Alumni toBeEdited);
    }

    public interface AchievementsMethods {
        void addNewAchievement(Achievements achievements);

        void deletenewAnnouncement(String achievementUUID);

        void editAchievement(Achievements currentAchievement, Achievements newAchievement);
    }



}

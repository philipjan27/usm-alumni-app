package usmalumni.emerien.com.usmalumniapp.authentication;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import usmalumni.emerien.com.usmalumniapp.activity.DashboardActivity;
import usmalumni.emerien.com.usmalumniapp.activity.RegisterActivity;

/**
 * Created by Philip on 01/06/2018.
 */

public class AuthPresenter implements AuthView.AuthCheckOnStart, AuthView.AuthMethod {

    public final static String TAG = AuthPresenter.class.getSimpleName();
    AuthView v;

    public AuthPresenter(AuthView v) {
        this.v=v;
    }


    @Override
    public void checkLogin(final Activity act, final FirebaseAuth auth, final String email, final String password) {
       Runnable r= new Runnable() {
           @Override
           public void run() {

               auth.signInWithEmailAndPassword(email,password).addOnCompleteListener(act, new OnCompleteListener<AuthResult>() {
                   @Override
                   public void onComplete(@NonNull Task<AuthResult> task) {
                       if (task.isSuccessful()) {
                           v.onSuccess(auth);
                           Log.e(TAG, "onComplete: Login Success");
                       }else {
                           v.onError();
                           Log.e(TAG, "onComplete: Login fail");
                       }
                   }
               }).addOnFailureListener(new OnFailureListener() {
                   @Override
                   public void onFailure(@NonNull Exception e) {
                       Log.e(TAG, "onFailure: "+e.getMessage().toString() );
                       v.onFailure();
                   }
               });

           }
       };
       Thread t= new Thread(r);
       t.start();
    }

    @Override
    public void checkIfAlreadyLoggedIn(FirebaseUser user, FirebaseAuth auth) {
        user= auth.getCurrentUser();
        if (user == null) {
            v.onNotAlreadyLogon();
        }else {
            v.onAlreadyLoggedIn(user);
        }

    }

    public void redirectToDashboard(Activity act,String role, String userUUID) {
        Intent intent= new Intent(act,DashboardActivity.class);
        intent.putExtra("role",role);
        intent.putExtra("uuid",userUUID);
        act.startActivity(intent);
    }

    public void redirectToRegistration(Activity act) {
        act.startActivity(new Intent(act, RegisterActivity.class));
    }

}

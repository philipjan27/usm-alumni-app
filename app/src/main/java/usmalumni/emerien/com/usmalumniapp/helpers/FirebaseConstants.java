package usmalumni.emerien.com.usmalumniapp.helpers;

/**
 * Created by Philip on 01/09/2018.
 */

public class FirebaseConstants {

    public static String ALUMNI_UUID="";
    public static String FACULTY_UUID="";
    public static String ADMIN_UUID="";

    public static String FIREBASE_STORAGE_ALUMNI="alumni/";

}

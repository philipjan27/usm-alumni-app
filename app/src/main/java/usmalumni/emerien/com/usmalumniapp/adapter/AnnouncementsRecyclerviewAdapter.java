package usmalumni.emerien.com.usmalumniapp.adapter;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import usmalumni.emerien.com.usmalumniapp.R;
import usmalumni.emerien.com.usmalumniapp.addingannouncements.AnnouncementDeletePresenter;
import usmalumni.emerien.com.usmalumniapp.addingannouncements.AnnouncementsRetrievePresenter;
import usmalumni.emerien.com.usmalumniapp.fragment.AnnouncementsFragment;
import usmalumni.emerien.com.usmalumniapp.helpers.AppUtils;
import usmalumni.emerien.com.usmalumniapp.model.Announcements;

/**
 * Created by Philip on 01/25/2018.
 */

public class AnnouncementsRecyclerviewAdapter extends RecyclerView.Adapter<AnnouncementsRecyclerviewAdapter.ViewHolder> implements AnnouncementsRetrievePresenter.RetrieveAnnouncementListCallback, AnnouncementDeletePresenter.DeleteInterfaceCallback {

    public final static String TAG = AnnouncementsRecyclerviewAdapter.class.getSimpleName();


    boolean isUserAdmin;
    ItemClickedCallback cb;
    List<Announcements> announcementsList = new ArrayList<>();
    View v;
    ProgressDialog pd;
    AnnouncementsRetrievePresenter retrievePresenter;
    AnnouncementDeletePresenter deletePresenter;
    AnnouncementsFragment fragment;



    public interface ItemClickedCallback {
        void editClicked(Announcements announcements, String itemUUID);

        void deleteClicked(int position, String itemUUID);
    }

    public AnnouncementsRecyclerviewAdapter(List<Announcements> announcements, ItemClickedCallback cb, boolean isUserAdmin) {
        this.announcementsList = announcements;
        this.cb = cb;
        this.isUserAdmin = isUserAdmin;
    }

    public AnnouncementsRecyclerviewAdapter(boolean isUserAdmin, ItemClickedCallback cb) {
        this.isUserAdmin = isUserAdmin;
        this.cb = cb;
        retrievePresenter = new AnnouncementsRetrievePresenter();
        deletePresenter = new AnnouncementDeletePresenter(this);
        retrievePresenter.getListOfAnnouncements();
        retrievePresenter.setRetrieveAnnouncementListCallback(this);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_announcements_itemlayout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        if (isUserAdmin) {
            holder.setIsRecyclable(false);
            if (this.announcementsList.size()-1 == position){
                holder.cardView.setCardElevation(16f);
                holder.newAnnouncementIndicator.setVisibility(View.VISIBLE);
            }

            holder.content.setText(announcementsList.get(position).getDescription());
            Picasso.with(v.getContext()).load(announcementsList.get(position).getImageFile()).centerCrop().resize(70,70).into(holder.uploadedImage);
            holder.datePosted.setText(convertTimeStampToReadableDate(announcementsList.get(position).getPublishDate()));
            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cb.editClicked(announcementsList.get(position), announcementsList.get(position).getUuid());
                    Log.e(TAG, "onClick: EDIT " + position);
                }
            });

            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cb.deleteClicked(position, announcementsList.get(position).getUuid());
                    Log.e(TAG, "onClick: DELETE " + position);
                    showAlertDialog(announcementsList.get(position));
                }
            });

            //          DISPLAY LARGE IMAGE
            holder.uploadedImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e(TAG, "onClick: CLICKED");
                    showImageDialog(announcementsList.get(position).getImageFile());
                }
            });

        } else {
            holder.setIsRecyclable(false);
            if (this.announcementsList.size()-1 == position){
                holder.cardView.setCardElevation(16f);
                holder.newAnnouncementIndicator.setVisibility(View.VISIBLE);
            }

            holder.content.setText(announcementsList.get(position).getDescription());
            Picasso.with(v.getContext()).load(announcementsList.get(position).getImageFile()).centerCrop().resize(70,70).into(holder.uploadedImage);
            holder.datePosted.setText(convertTimeStampToReadableDate(announcementsList.get(position).getPublishDate()));

//           EDIT
            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cb.editClicked(announcementsList.get(position), announcementsList.get(position).getUuid());
                    Log.e(TAG, "onClick: EDIT " + position);
                }
            });
//          DELETE
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cb.deleteClicked(position, announcementsList.get(position).getUuid());
                    Log.e(TAG, "onClick: DELETE " + position);
                }
            });
//          DISPLAY LARGE IMAGE
            holder.uploadedImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e(TAG, "onClick: CLICKED");
                    showImageDialog(announcementsList.get(position).getImageFile());
                }
            });

            holder.edit.setVisibility(View.GONE);
            holder.delete.setVisibility(View.GONE);
        }

        Log.e(TAG, "onBindViewHolder: " + announcementsList.get(position).getImageFile());

    }

    @Override
    public int getItemCount() {
        return announcementsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView datePosted, content;
        ImageView uploadedImage;
        Button edit, delete;
        LinearLayout newAnnouncementIndicator;

        public ViewHolder(View itemView) {
            super(itemView);
            newAnnouncementIndicator= (LinearLayout)itemView.findViewById(R.id.linearlayout_newupdateindicator);
            cardView= (CardView) itemView.findViewById(R.id.cardview_announcement);
            datePosted = (TextView) itemView.findViewById(R.id.textview_dateposted);
            content = (TextView) itemView.findViewById(R.id.textview_content);
            uploadedImage = (ImageView) itemView.findViewById(R.id.imageView_announcementImage);
            edit = (Button) itemView.findViewById(R.id.button_announcement_edit);
            delete = (Button) itemView.findViewById(R.id.button_announcement_delete);

        }

    }

    private String convertTimeStampToReadableDate(long ts) {
        String result = "";
        Date date = new Date(ts);
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
        result = sdf.format(date);
        return result;
    }

    @Override
    public void getAnnouncementList(List<Announcements> announcements) {

        if (announcements.size() > this.announcementsList.size()){
            Log.e(TAG, "getAnnouncementList: Size update!" );
            notifyDataSetChanged();
        }

        this.announcementsList=announcements;
        notifyDataSetChanged();
    }

    private void showAlertDialog(final Announcements obj) {
        pd = new ProgressDialog(v.getContext());
        pd.setTitle("Announcement");
        pd.setMessage("Deleting data");
        pd.setCanceledOnTouchOutside(false);
        pd.setCancelable(false);


        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext())
                .setTitle("Announcement")
                .setMessage("Delete this item ?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                      if (AppUtils.isDeviceConnectedToAnyNetworks(v.getContext())){
                          pd.show();
                          int pos = announcementsList.indexOf(obj);
                          deletePresenter.deleteAnnouncement(obj.getUuid());
                          deletePresenter.deleteImageFile(obj.getLocalImageFileName());
                          announcementsList.remove(pos);
                          notifyDataSetChanged();
                      }else {
                          Toasty.error(v.getContext(),"No internet connection. Please try again",Toast.LENGTH_SHORT,true).show();
                      }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    //    DELETE INTERFACE CALLBACK
    @Override
    public void announcementDeletionSuccess() {
        pd.dismiss();
    }

    @Override
    public void announcementDeletionError() {
        pd.dismiss();
        Toasty.error(v.getContext(), "An error occured while deleting data", Toast.LENGTH_LONG, true).show();
    }

    @Override
    public void announcementImageDeleteSuccess() {
        Toasty.success(v.getContext(), "Success in deleting image file", Toast.LENGTH_LONG, true).show();
    }

    @Override
    public void announcementImageDeleteFailure() {
        Toasty.error(v.getContext(), "Success in deleting image file", Toast.LENGTH_LONG, true).show();

    }

    private void showImageDialog(String imageUrl) {

        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        LayoutInflater inflater = (LayoutInflater) v.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ImageView imageUploaded;
        View imageDialog = inflater.inflate(R.layout.show_image_largescaledialog, null, false);
        imageUploaded = imageDialog.findViewById(R.id.imageview_largeProfile);
        builder.setView(imageDialog);

        Picasso.with(imageDialog.getContext()).load(imageUrl).into(imageUploaded);

        AlertDialog dialog = builder.create();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }
}

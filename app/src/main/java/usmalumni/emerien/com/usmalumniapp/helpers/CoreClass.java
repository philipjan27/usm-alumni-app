package usmalumni.emerien.com.usmalumniapp.helpers;

import android.app.AlertDialog;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.provider.Settings;

/**
 * Created by Philip on 03/19/2018.
 */

public class CoreClass extends Application {

    InternetConnectionBroadcastReciever receiver;
    IntentFilter intentFilter;

    @Override
    public void onCreate() {
        super.onCreate();
        receiver = new InternetConnectionBroadcastReciever();
        intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(receiver, intentFilter);
    }

    public void unregisterBroadcastReciever() {
        unregisterReceiver(receiver);
    }



}

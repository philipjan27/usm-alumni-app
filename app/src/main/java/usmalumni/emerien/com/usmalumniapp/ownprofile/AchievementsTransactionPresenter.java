package usmalumni.emerien.com.usmalumniapp.ownprofile;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import usmalumni.emerien.com.usmalumniapp.model.Achievements;

/**
 * Created by Philip on 02/25/2018.
 */

public class AchievementsTransactionPresenter implements ProfileView.AchievementsMethods {

    public final static String TAG = AchievementsTransactionPresenter.class.getSimpleName();

    AchievementDeleteCallback achievementDeleteCallback;
    AchievementEditCallback achievementEditCallback;
    AchievementAddCallback addAchievementCb;
    DatabaseReference ref, toBeEdited;


    public AchievementsTransactionPresenter() {

        ref = FirebaseDatabase.getInstance().getReference().child("Achievements");
    }

    @Override
        public void addNewAchievement(Achievements achievements) {
            final Achievements achievementsRef = achievements;
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    achievementsRef.setUuid(ref.push().getKey());
                    ref.child(achievementsRef.getUuid()).setValue(achievementsRef)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    addAchievementCb.onSuccessAdd();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    addAchievementCb.onFailureAdd();
                                }
                            });
                }
            });

            t.start();
        }

    @Override
    public void deletenewAnnouncement(final String achievementUUID) {
        final String uuidValue = achievementUUID;
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                ref.child(uuidValue).removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                achievementDeleteCallback.onSuccessDelete();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                achievementDeleteCallback.onFailureDelete();
                            }
                        });
            }
        });

        t.start();
    }

    @Override
    public void editAchievement(final Achievements currentAchievement, final Achievements newAchievement) {

        final Achievements achievementsReference=currentAchievement;

        Thread t= new Thread(new Runnable() {
            @Override
            public void run() {

                toBeEdited = ref.child(achievementsReference.getUuid());
                toBeEdited.setValue(newAchievement)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.e(TAG, "onSuccess: SUCCESS EDITING EXISTING ACHIEVEMENT");
                                achievementEditCallback.onSuccessEdit();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.e(TAG, "onFailure: FAILED TO EDIT ACHIEVEMENT");
                                achievementEditCallback.onFailureEdit();
                            }
                        });


            }
        });

        t.start();
    }


    public void setAchievementEditCallbackListeners(AchievementEditCallback cb) {
        this.achievementEditCallback = cb;
    }

    public void setAchievementDeleteCallbackListener(AchievementDeleteCallback callback) {
        this.achievementDeleteCallback = callback;
    }

    public void setAchievementAddCallbackListener(AchievementAddCallback addCallback){
        this.addAchievementCb=addCallback;
    }

    public interface AchievementEditCallback {
        void onSuccessEdit();

        void onFailureEdit();
    }

    public interface AchievementDeleteCallback {
        void onSuccessDelete();

        void onFailureDelete();
    }

    public interface AchievementAddCallback{
        void onSuccessAdd();
        void onFailureAdd();
    }
}

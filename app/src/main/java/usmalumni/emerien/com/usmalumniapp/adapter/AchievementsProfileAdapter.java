package usmalumni.emerien.com.usmalumniapp.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;
import java.util.List;

import usmalumni.emerien.com.usmalumniapp.R;
import usmalumni.emerien.com.usmalumniapp.model.Achievements;
import usmalumni.emerien.com.usmalumniapp.ownprofile.AchievementsTransactionPresenter;
import usmalumni.emerien.com.usmalumniapp.ownprofile.ProfileRetrievePresenter;

/**
 * Created by Philip on 02/25/2018.
 */

public class AchievementsProfileAdapter extends ArrayAdapter<Achievements> {

    public final static String TAG = AchievementsProfileAdapter.class.getSimpleName();

    ProgressDialog pd;
    List<Achievements> achievementsList;
    Context ctx;
    View v;
    int selectedPOS;
    Achievements newAchievement;
    boolean isListpdating=false;
    boolean hideEditDelete;

//  ACHIEVEMENT EDIT DELETE LISTENER
    OnAchievementEditOrDeleteCallbackListener achievementEditDeletelistener;


    private static class ViewHolder {
        TextView achivementDesc;
        ImageView deleteAchivement,editAchievement;
    }


    public AchievementsProfileAdapter(Context ctx,String uuid,List<Achievements> achievementsListConst, boolean hideEditDelete) {
        super(ctx, R.layout.custom_achievement_layout);
        this.ctx=ctx;
        this.achievementsList=achievementsListConst;
        this.hideEditDelete=hideEditDelete;
    }

    @Override
    public int getCount() {
        return achievementsList.size();
    }

    @Nullable
    @Override
    public Achievements getItem(int position) {
        return achievementsList.get(position);
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder= new ViewHolder();
            LayoutInflater inflater=LayoutInflater.from(ctx);
            v=inflater.inflate(R.layout.custom_profile_achievement_layout,parent,false);


            holder.achivementDesc= v.findViewById(R.id.textview_achivement);
            holder.deleteAchivement= v.findViewById(R.id.achievement_imageview_delete);
            holder.editAchievement= v.findViewById(R.id.achievement_imageview_edit_achievement);

            v.setTag(holder);
        }else {
            holder= (ViewHolder)v.getTag();
        }

//      set the edit and delete to gone so that il transfer the edit and delete method to the editprofile fragment
        if (hideEditDelete) {
            holder.editAchievement.setVisibility(View.GONE);
            holder.deleteAchivement.setVisibility(View.GONE);
        }


        holder.achivementDesc.setText(achievementsList.get(position).getAchievementDetails());
        holder.deleteAchivement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPOS=position;
//                showDialog(achievementsList.get(position));
                achievementEditDeletelistener.onDeleteSelected(achievementsList.get(position));
            }
        });

        holder.editAchievement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPOS=position;
//                showEditAchievementDialog(achievementsList.get(position),position);
                achievementEditDeletelistener.onEditSelected(achievementsList.get(position));
            }
        });


        return v;
    }

    public void setAchievementEditDeleteCallbackListener(OnAchievementEditOrDeleteCallbackListener cb) {
        this.achievementEditDeletelistener=cb;
    }

    /**
     * INTERFACE FOR EDITING/DELETING ACHIEVEMENTS
     */
    public interface OnAchievementEditOrDeleteCallbackListener{
        void onEditSelected(Achievements toBeEdited);
        void onDeleteSelected(Achievements toBeDeleted);
    }
}

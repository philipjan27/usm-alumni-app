package usmalumni.emerien.com.usmalumniapp.searchprofile;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import usmalumni.emerien.com.usmalumniapp.model.Alumni;

/**
 * Created by Philip on 02/17/2018.
 */

public class SearchUserRetrievePresenter implements SearchUserView.RetrieveAllUsers {


    DatabaseReference ref;
    GetAllUsersCallback cb;
    List<Alumni> alumniList;

    public SearchUserRetrievePresenter() {

        ref= FirebaseDatabase.getInstance().getReference().child("Alumni");
    }

    @Override
    public void getAllUsers() {
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                alumniList= new ArrayList<>();

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Alumni alumni= ds.getValue(Alumni.class);
                    alumniList.add(alumni);
                }

                cb.getSingleAlumniData(alumniList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setGetAllUsersCallbackListener(GetAllUsersCallback cb){
        this.cb=cb;
    }

    public interface GetAllUsersCallback{
        void getSingleAlumniData(List<Alumni> alumniList);
    }
}

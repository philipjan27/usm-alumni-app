package usmalumni.emerien.com.usmalumniapp.jobhirings;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import usmalumni.emerien.com.usmalumniapp.model.Jobs;

/**
 * Created by Philip on 02/17/2018.
 */

public class EditJobHiringPresenter implements JobHiringView.EditJobHiring {

    public final static String TAG = EditJobHiringPresenter.class.getSimpleName();

    DatabaseReference ref;
    JobHiringEditCallback cb;

    public EditJobHiringPresenter(JobHiringEditCallback cb) {
        this.cb = cb;
    }

    @Override
    public void editJobHiring(final Jobs selectedJobs, final Jobs newCreatedJob) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                ref = FirebaseDatabase.getInstance().getReference().child("Jobs").child(selectedJobs.getUuid());
                ref.setValue(newCreatedJob)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.e(TAG, "onSuccess: EDIT JOB SUCCESS" );
                                cb.successJobHiringEdit();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.e(TAG, "onFailure: EDIT JOB SUCCESS" );
                                cb.failedJobHiringEdit();
                            }
                        });
            }
        });

        t.start();
    }

    public interface JobHiringEditCallback {
        void successJobHiringEdit();

        void failedJobHiringEdit();
    }
}

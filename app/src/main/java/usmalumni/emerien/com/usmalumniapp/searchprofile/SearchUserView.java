package usmalumni.emerien.com.usmalumniapp.searchprofile;

import usmalumni.emerien.com.usmalumniapp.model.Alumni;

/**
 * Created by Philip on 02/17/2018.
 */

public interface SearchUserView {

    public interface RetrieveAllUsers{
        void getAllUsers();
    }
}

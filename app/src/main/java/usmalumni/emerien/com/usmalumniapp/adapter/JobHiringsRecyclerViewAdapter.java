package usmalumni.emerien.com.usmalumniapp.adapter;

        import android.app.AlertDialog;
        import android.app.ProgressDialog;
        import android.content.DialogInterface;
        import android.support.v7.widget.CardView;
        import android.support.v7.widget.RecyclerView;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.Button;
        import android.widget.LinearLayout;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.google.firebase.database.DataSnapshot;
        import com.google.firebase.database.DatabaseError;

        import java.text.SimpleDateFormat;
        import java.util.ArrayList;
        import java.util.Date;
        import java.util.List;

        import es.dmoral.toasty.Toasty;
        import usmalumni.emerien.com.usmalumniapp.R;
        import usmalumni.emerien.com.usmalumniapp.helpers.AppUtils;
        import usmalumni.emerien.com.usmalumniapp.jobhirings.DeleteJobHiringPresenter;
        import usmalumni.emerien.com.usmalumniapp.jobhirings.RetrieveJobHiringsPresenter;
        import usmalumni.emerien.com.usmalumniapp.model.Jobs;

/**
 * Created by Philip on 02/13/2018.
 */

//public class JobHiringsRecyclerViewAdapter extends RecyclerView.Adapter<JobHiringsRecyclerViewAdapter.ViewHolder> implements RetrieveJobHiringsPresenter.GetListOfJobsList, DeleteJobHiringPresenter.DeleteInterfaceCallback {
public class JobHiringsRecyclerViewAdapter extends RecyclerView.Adapter<JobHiringsRecyclerViewAdapter.ViewHolder> implements RetrieveJobHiringsPresenter.GetListOfJobsListCallback, DeleteJobHiringPresenter.DeleteInterfaceCallback {

    public final static String TAG = JobHiringsRecyclerViewAdapter.class.getSimpleName();
    ItemClickedCallack cb;
    View v;
    boolean isUserAdmin;
    List<Jobs> jobsList = new ArrayList<>();
    ProgressDialog pd;
    RetrieveJobHiringsPresenter retrievePresenter;
    DeleteJobHiringPresenter deleteJobHiringPresenter;

    public JobHiringsRecyclerViewAdapter(boolean isUserAdmin, ItemClickedCallack cb) {
        this.isUserAdmin = isUserAdmin;
        this.cb = cb;
//        retrievePresenter= new RetrieveJobHiringsPresenter(this);
        retrievePresenter= new RetrieveJobHiringsPresenter();
        retrievePresenter.getJobsList();
        retrievePresenter.setGetListOfJobsListCallback(this);
        deleteJobHiringPresenter= new DeleteJobHiringPresenter(this);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_jobfragment_itemlayout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (isUserAdmin) {
            holder.setIsRecyclable(false);
            if (this.jobsList.size()-1 == position){
                holder.cardView.setCardElevation(16f);
                holder.newJobHiringIndicator.setVisibility(View.VISIBLE);
            }

            holder.content.setText(jobsList.get(position).getJobPositionDescription());
            holder.jobPosition.setText(jobsList.get(position).getJobPositionName());
            holder.datePublished.setText(convertTimeStampToReadableDate(jobsList.get(position).getDatePostedTimeStamp()));

            holder.editBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cb.onEditClicked(jobsList.get(position));
                    Log.e(TAG, "onClick: EDIT POSITION: "+position);
                }
            });

            holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cb.onDeleteClicked(jobsList.get(position));
                    Log.e(TAG, "onClick: DELETE POSITION: "+position);
                    showDialog(jobsList.get(position));
                }
            });
        } else {
            holder.setIsRecyclable(false);
            if (this.jobsList.size()-1 == position){
                holder.cardView.setCardElevation(16f);
                holder.newJobHiringIndicator.setVisibility(View.VISIBLE);
            }

            holder.content.setText(jobsList.get(position).getJobPositionDescription());
            holder.jobPosition.setText(jobsList.get(position).getJobPositionName());
            holder.datePublished.setText(convertTimeStampToReadableDate(jobsList.get(position).getDatePostedTimeStamp()));

            holder.editBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cb.onEditClicked(jobsList.get(position));
                    Log.e(TAG, "onClick: EDIT POSITION: "+position);
                }
            });

            holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cb.onDeleteClicked(jobsList.get(position));
                    Log.e(TAG, "onClick: DELETE POSITION: "+position);
                    showDialog(jobsList.get(position));
                }
            });

            holder.editBtn.setVisibility(View.GONE);
            holder.deleteBtn.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return jobsList.size();
    }




    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView datePublished, content, jobPosition;
        Button editBtn, deleteBtn;
        CardView cardView;
        LinearLayout newJobHiringIndicator;

        public ViewHolder(View itemView) {
            super(itemView);
            newJobHiringIndicator= (LinearLayout)  itemView.findViewById(R.id.linearlayout_newupdateindicator);
            cardView= (CardView) itemView.findViewById(R.id.cardview_jobhiring);
            datePublished = itemView.findViewById(R.id.jobhiring_dateposted_tv);
            content = itemView.findViewById(R.id.jobhiring_content);
            jobPosition = itemView.findViewById(R.id.positionName);
            editBtn = itemView.findViewById(R.id.jobhiring_edit);
            deleteBtn = itemView.findViewById(R.id.jobhiring_delete);
        }
    }

    private String convertTimeStampToReadableDate(long ts) {
        StringBuilder builder = new StringBuilder();
        String result = "";
        String value;
        Date date = new Date(ts);
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
        result = sdf.format(date);
        value = builder.append("(").append(result).append(")").toString();
        return value;
    }

    private void showDialog(final Jobs jobs){

        pd = new ProgressDialog(v.getContext());
        pd.setTitle("Job Hiring");
        pd.setMessage("Deleting data");
        pd.setCanceledOnTouchOutside(false);
        pd.setCancelable(false);

        AlertDialog.Builder builder= new AlertDialog.Builder(v.getContext())
                .setTitle("Job Hiring")
                .setMessage("Delete this item ?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (AppUtils.isDeviceConnectedToAnyNetworks(v.getContext())){
                            pd.show();
                            int pos= jobsList.indexOf(jobs);
                            deleteJobHiringPresenter.deleteJobHiring(jobs.getUuid());
                            jobsList.remove(pos);
                            notifyDataSetChanged();
                        }else {
                            Toasty.error(v.getContext(),"No internet connection. Please try again",Toast.LENGTH_SHORT,true).show();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog= builder.create();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public interface ItemClickedCallack {
        void onEditClicked(Jobs job);
        void onDeleteClicked(Jobs job);
    }


    /**
     * RETRIEVE LIST OF JOB HIRINGS ADDED IN FIREBASE
     */

    @Override
    public void getListOfJobsList(List<Jobs> jobsList) {
        this.jobsList=jobsList;
        notifyDataSetChanged();
    }

   /* @Override
    public void onChildAdded(DataSnapshot ds) {
        Jobs j = ds.getValue(Jobs.class);
        jobsList.add(j);
        notifyDataSetChanged();
        Log.e(TAG, "onChildAdded: ADDED: "+j.getJobPositionName() );
    }

    @Override
    public void onChildChanged(DataSnapshot ds) {
        Jobs j= ds.getValue(Jobs.class);
        for (int i=0; i<jobsList.size();i++) {
            if (j.getUuid().equals(jobsList.get(i).getUuid())) {
                jobsList.set(i,j);
                notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onChildRemoved(DataSnapshot ds) {

    }

    @Override
    public void onChildMoved(DataSnapshot ds) {

    }

    @Override
    public void onCancelled(DatabaseError err) {

    }*/

    /**
     * DELETE CALLBACK
     */
    @Override
    public void jobHiringDeleteSuccess() {
        pd.dismiss();
        Toasty.success(v.getContext(), "Success in deleting item", Toast.LENGTH_LONG, true).show();
    }

    @Override
    public void jobHiringDeleteFailed() {
        pd.dismiss();
        Toasty.error(v.getContext(),"Error deleting item", Toast.LENGTH_LONG,true).show();
    }
}

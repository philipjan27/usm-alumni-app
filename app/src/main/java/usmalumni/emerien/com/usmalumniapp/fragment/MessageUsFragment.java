package usmalumni.emerien.com.usmalumniapp.fragment;


import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.creativityapps.gmailbackgroundlibrary.BackgroundMail;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import usmalumni.emerien.com.usmalumniapp.R;
import usmalumni.emerien.com.usmalumniapp.helpers.SharedPreferenceHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessageUsFragment extends DialogFragment implements BackgroundMail.OnSuccessCallback, BackgroundMail.OnFailCallback {

    public static String TAG=MessageUsFragment.class.getSimpleName();


    SharedPreferences sharedPreferences;
    SharedPreferenceHelper sharedPreferenceHelper;
    private String currEmail;
    private View v;

    @BindView(R.id.messageus_subject)
    AppCompatEditText messageusSubject;

    @BindView(R.id.messageud_messagecontent)
    AppCompatEditText messageusContent;

    @BindView(R.id.messageus_send)
    AppCompatButton messageusSend;

    @BindView(R.id.messageus_cancel)
    AppCompatButton messageusCancel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferenceHelper = SharedPreferenceHelper.getInstance();
        sharedPreferences = sharedPreferenceHelper.initializePrefs(getActivity());
        currEmail = sharedPreferenceHelper.getCurrentUserEmail(sharedPreferences);setStyle(DialogFragment.STYLE_NORMAL,R.style.dialog_theme);
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d!=null){
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            d.getWindow().setLayout(width, height);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_message_us, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick(R.id.messageus_send)
    public void sendConcernViaEmail(View v) {
        if (!messageusSubject.getText().toString().isEmpty() && !messageusContent.getText().toString().isEmpty() && !currEmail.isEmpty()) {

            String emailMessageContent="A message from USM Alumni App user: "+currEmail+"\n\n\'"+messageusContent.getText().toString()+"'\n\n\n\n***Please do not reply in this automated mail***";

            BackgroundMail.newBuilder(getActivity())
                    .withUsername(getActivity().getResources().getString(R.string.mailsenderemailadd))
                    .withType(BackgroundMail.TYPE_PLAIN)
                    .withPassword(getActivity().getResources().getString(R.string.mailsenderemailpass))
                    .withMailto(getActivity().getResources().getString(R.string.admin_email))
                    .withSubject(messageusSubject.getText().toString())
                    .withBody(emailMessageContent)
                    .withOnSuccessCallback(this)
                    .withOnFailCallback(this).send();
        } else {
            messageusSubject.setError("Please provide subject");
            messageusContent.setError("Please provide content");
        }
    }

    @OnClick(R.id.messageus_cancel)
    public void cancelEmailConcern(View v) {
        dismiss();
    }


    @Override
    public void onFail() {
        Log.e(TAG, "onFail: EMAIL FAILED TO SEND EMAIL");
    }

    @Override
    public void onSuccess() {
        Log.e(TAG, "onFail: EMAIL SUCCESS TO SEND EMAIL");
        getActivity().onBackPressed();
        dismiss();
    }
}

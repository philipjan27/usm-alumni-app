package usmalumni.emerien.com.usmalumniapp.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

/**
 * Created by Philip on 02/21/2018.
 */

public class AppUtils {

    public static boolean isDeviceConnectedToAnyNetworks(Context ctx) {
        ConnectivityManager cm= (ConnectivityManager)ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo= cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

}
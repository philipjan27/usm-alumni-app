package usmalumni.emerien.com.usmalumniapp.addingannouncements;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import usmalumni.emerien.com.usmalumniapp.helpers.FirebaseUtilities;
import usmalumni.emerien.com.usmalumniapp.model.Announcements;

/**
 * Created by Philip on 01/21/2018.
 */

public class AnnouncementsAddPresenter implements AnnouncementsView.AddAnnouncements {

    public final static String TAG = AnnouncementsAddPresenter.class.getSimpleName();

    Context context;
    FirebaseUtilities fbUtils;
    AnnouncementsView v;

    public AnnouncementsAddPresenter(AnnouncementsView vx, Context ctx) {
        this.context = ctx;
        fbUtils = FirebaseUtilities.getInstance(ctx);
        this.v = vx;
    }


    @Override
    public void uploadImageToFbStorage(Announcements announcements, Uri imageUri) {


        uploadAnnouncementImage(announcements, imageUri);

    }

    private void uploadAnnouncementImage(final Announcements announcements, Uri uri) {

        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Announcements");
        announcements.setUuid(ref.push().getKey());


        if (uri.toString().equals("") || uri.toString().isEmpty() || uri == null) {
            announcements.setImageFile(uri.toString());
            announcements.setLocalImageFileName(announcements.getUuid() + "." + fbUtils.getImageExtension(uri, context));
            saveDatatoRealtimeDb(ref, announcements);
            Log.e(TAG, "uploadAnnouncementImage: Null Image");
        } else {

            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("announcements/" + announcements.getUuid() + "." + fbUtils.getImageExtension(uri, context));
            announcements.setLocalImageFileName(announcements.getUuid() + "." + fbUtils.getImageExtension(uri, context));
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Log.e(TAG, "onSuccess: Download URL: " + taskSnapshot.getDownloadUrl());
                            announcements.setImageFile(taskSnapshot.getDownloadUrl().toString());
                            saveDatatoRealtimeDb(ref, announcements);
                            v.imageUploadSuccess();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e(TAG, "onFailure: Failed in uploading image to storage" + "\n" + e.getMessage());
                            v.imageUploadError(e);
                        }
                    });
        }

    }

    private void saveDatatoRealtimeDb(DatabaseReference ref, Announcements announcements) {

        ref.child(announcements.getUuid()).setValue(announcements)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        v.addAnnouncementSuccess();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                v.errorAnnoucementPublish();
            }
        });

    }


}

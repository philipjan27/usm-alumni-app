package usmalumni.emerien.com.usmalumniapp.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import usmalumni.emerien.com.usmalumniapp.R;
import usmalumni.emerien.com.usmalumniapp.dashboard.DashboardPresenter;
import usmalumni.emerien.com.usmalumniapp.dashboard.DashboardView;
import usmalumni.emerien.com.usmalumniapp.fragment.AnnouncementsFragment;
import usmalumni.emerien.com.usmalumniapp.fragment.JobHiringFragment;
import usmalumni.emerien.com.usmalumniapp.fragment.MessageUsFragment;
import usmalumni.emerien.com.usmalumniapp.fragment.ProfileFragment;
import usmalumni.emerien.com.usmalumniapp.fragment.UsersFragment;
import usmalumni.emerien.com.usmalumniapp.helpers.CoreClass;
import usmalumni.emerien.com.usmalumniapp.helpers.SharedPreferenceHelper;
import usmalumni.emerien.com.usmalumniapp.model.Alumni;
import usmalumni.emerien.com.usmalumniapp.model.Announcements;

public class DashboardActivity extends AppCompatActivity implements DashboardView {

    public final static String TAG = DashboardActivity.class.getSimpleName();

    DashboardPresenter presenter;

    String[] navMenuForAdmin = new String[]{"Announcements", "Job Hirings", "Users", "Profile", "Logout"};
    String[] navMenuForFaculty = new String[]{"Announcements", "Job Hirings", "Users", "Profile", "Message us", "Logout"};
    String[] navMenuForAlumni = new String[]{"Announcements", "Job Hirings", "Profile", "Message us", "Logout"};
    ArrayAdapter<String> arrayAdapter;

    Bundle bundle;
    Toolbar toolbar;
    String role, userUUID;
    NavigationView navView;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle toggle;
    ListView lv;
    FrameLayout fragmentContainer;

    View navHeader;
    CircleImageView profileImageview;
    TextView fullname;
    TextView email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        initToolbar();

        presenter = new DashboardPresenter(this);

        navHeader = findViewById(R.id.navHeader);
        profileImageview = navHeader.findViewById(R.id.dashboard_profilepic);
        fullname = navHeader.findViewById(R.id.dashboard_fullname);
        email = navHeader.findViewById(R.id.dashboard_email);


        fragmentContainer = findViewById(R.id.dashboard_container);
        navView = findViewById(R.id.navigatiobView);
        drawerLayout = findViewById(R.id.drawerlayout);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open_drawer, R.string.close_drawer);
        drawerLayout.addDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        drawerLayout.openDrawer(Gravity.LEFT);
        lv = findViewById(R.id.listview_menu);

        getExtraRoleFile();
    }

    @Override
    protected void onResume() {
        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return false;
            }
        });
        setupnavigationMenuBaseOnCurrentLogonRole(role);
        super.onResume();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ((CoreClass) getApplication()).unregisterBroadcastReciever();
    }

    @Override
    public void onBackPressed() {
        Log.e(TAG, "onBackPressed: BACKSTACK COUNT " + getFragmentManager().getBackStackEntryCount());
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    private void getExtraRoleFile() {
        bundle = getIntent().getExtras();
        if (bundle != null) {
            role = bundle.getString("role");
            userUUID = bundle.getString("uuid");
            presenter.getProfileDetails(userUUID);
            Log.e(TAG, "onCreate: BUNDLE= " + role);
            Log.e(TAG, "onCreate: BUNDLE= " + userUUID);
        } else {
            Log.e(TAG, "onCreate: BUNDLE EMPTY!");
        }
    }

    private void createExitAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("USM Alumni App")
                .setMessage("Are you sure you want to exit ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        System.exit(0);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    private void initToolbar() {
        toolbar = findViewById(R.id.dashboard_toolbar);
        toolbar.setTitle("Dashboard");
        toolbar.setTitleTextColor(getResources().getColor(R.color.accent));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.dashboard_logout:
                doLogout();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    //    Logout and clear all shared prefs data
    private void doLogout() {
        SharedPreferenceHelper helper = SharedPreferenceHelper.getInstance();
        SharedPreferences prefs = helper.initializePrefs(this);
        helper.cleanPrefs(prefs);
        createLogoutAlertDialog();
    }

    //    Create logout alert dialog
    private void createLogoutAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("USM Alumni app")
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(DashboardActivity.this, MainActivity.class));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    private void setupnavigationMenuBaseOnCurrentLogonRole(String role) {

        String matchedRole = "";
        String[] selectedList = null;

        switch (role) {
            case "Admin":
                matchedRole = "Admin";
                selectedList = navMenuForAdmin.clone();
                arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, navMenuForAdmin);
                lv.setAdapter(arrayAdapter);
                break;
            case "Faculty":
                matchedRole = "Faculty";
                selectedList = navMenuForFaculty.clone();
                arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, navMenuForFaculty);
                lv.setAdapter(arrayAdapter);
                break;
            case "Alumni":
                matchedRole = "Alumni";
                selectedList = navMenuForAlumni.clone();
                arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, navMenuForAlumni);
                lv.setAdapter(arrayAdapter);
                break;
        }


        final String finalMatchedRole = matchedRole;
        final String[] finalSelectedList = selectedList;

//      SET DEFAULT FRAGMENT
        gotoSelectedFragment(AnnouncementsFragment.class, "default");
        toolbar.setTitle("Announcements");


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (finalMatchedRole) {
                    case "Admin":
                        switch (finalSelectedList[i].toString()) {
                            case "Announcements":
                                if (finalSelectedList[i].equals("Announcements")) {
                                    Log.e(TAG, "onItemClick: ADMIN-Announcements");
                                    toolbar.setTitle("Announcements");
                                    gotoSelectedFragment(AnnouncementsFragment.class, "admin_annoucements");
                                }
                                break;
                            case "Job Hirings":
                                if (finalSelectedList[i].equals("Job Hirings")) {
                                    Log.e(TAG, "onItemClick: ADMIN-Job-Hirings");
                                    toolbar.setTitle("Job Hirings");
                                    gotoSelectedFragment(JobHiringFragment.class, "admin_jobhiring");
                                }
                                break;
                            case "Users":
                                if (finalSelectedList[i].equals("Users")) {
                                    toolbar.setTitle("Users");
                                    Log.e(TAG, "onItemClick: ADMIN-Users");
                                    gotoSelectedFragment(UsersFragment.class, "search_user");
                                }
                                break;
                            case "Profile":
                                if (finalSelectedList[i].equals("Profile")) {
                                    toolbar.setTitle("Profile");
                                    gotoSelectedFragment(ProfileFragment.class, "profile");
                                    Log.e(TAG, "onItemClick: ADMIN-Profile");
                                }
                                break;
                            case "Logout":
                                if (finalSelectedList[i].equals("Logout")) {
                                    doLogout();
                                }
                                break;
                        }
                        break;
                    case "Faculty":
                        switch (finalSelectedList[i].toString()) {
                            case "Announcements":
                                if (finalSelectedList[i].equals("Announcements")) {
                                    Log.e(TAG, "onItemClick: FACULTY-Annoucements");
                                    toolbar.setTitle("Announcements");
                                    gotoSelectedFragment(AnnouncementsFragment.class, "admin_annoucements");
                                }
                                break;
                            case "Job Hirings":
                                if (finalSelectedList[i].equals("Job Hirings")) {
                                    Log.e(TAG, "onItemClick: FACULTY-Job Hirings");
                                    toolbar.setTitle("Job Hirings");
                                    gotoSelectedFragment(JobHiringFragment.class, "job_hiring");
                                }
                                break;
                            case "Users":
                                if (finalSelectedList[i].equals("Users")) {
                                    Log.e(TAG, "onItemClick: FACULTY-Users");
                                    toolbar.setTitle("Users");
                                    gotoSelectedFragment(UsersFragment.class, "search_user");
                                }
                                break;
                            case "Profile":
                                if (finalSelectedList[i].equals("Profile")) {
                                    Log.e(TAG, "onItemClick: FACULTY-Profile");
                                    toolbar.setTitle("Profile");
                                    gotoSelectedFragment(ProfileFragment.class, "profile");
                                }
                                break;

                            case "Message us":
                                if (finalSelectedList[i].equals("Message us")) {
                                    Log.e(TAG, "onItemClick: FACULTY-Message us");
                                    MessageUsFragment messageUsFragment= new MessageUsFragment();
                                    messageUsFragment.setCancelable(false);
                                    messageUsFragment.show(getSupportFragmentManager(),"MESSAGEUS");
                                }
                                break;
                            case "Logout":
                                if (finalSelectedList[i].equals("Logout")) {
                                    doLogout();
                                }
                        }
                        break;
                    case "Alumni":
                        switch (finalSelectedList[i].toString()) {
                            case "Announcements":
                                if (finalSelectedList[i].equals("Announcements")) {
                                    Log.e(TAG, "onItemClick: Alumni-Annoucements");
                                    toolbar.setTitle("Announcements");
                                    gotoSelectedFragment(AnnouncementsFragment.class, "admin_annoucements");
                                }
                                break;
                            case "Job Hirings":
                                if (finalSelectedList[i].equals("Job Hirings")) {
                                    Log.e(TAG, "onItemClick: Alumni-Job Hirings");
                                    toolbar.setTitle("Job Hirings");
                                    gotoSelectedFragment(JobHiringFragment.class, "job_hiring");
                                }
                                break;
                            case "Profile":
                                if (finalSelectedList[i].equals("Profile")) {
                                    toolbar.setTitle("Profile");
                                    Log.e(TAG, "onItemClick: Alumni-Profile");
                                    gotoSelectedFragment(ProfileFragment.class, "profile");
                                }
                                break;
                            case "Message us":
                                if (finalSelectedList[i].equals("Message us")) {
                                    Log.e(TAG, "onItemClick: ALUMNI-Message us");
                                    MessageUsFragment messageUsFragment=new MessageUsFragment();
                                    messageUsFragment.setCancelable(false);
                                    messageUsFragment.show(getSupportFragmentManager(),"MESSAGEUS");
                                }
                                break;

                            case "Logout":
                                if (finalSelectedList[i].equals("Logout")) {
                                    doLogout();
                                }
                        }
                        break;
                }
            }
        });

    }


    public void gotoSelectedFragment(Class fragmentClass, String backstack) {

        Fragment frag = null;

        try {
            frag = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.dashboard_container, frag);
        ft.addToBackStack(backstack);
        ft.commit();

        drawerLayout.closeDrawers();

    }


    @Override
    public void retrieveRoleProfileDetails(Alumni alumni) {

        try {

            if (alumni.getImageFile().isEmpty() || alumni.getImageFile().equals("")) {
                Picasso.with(this).load(R.drawable.ic_emptyprofilepic).centerCrop().resize(70, 70).onlyScaleDown().into(profileImageview);
            } else {
                Picasso.with(this).load(alumni.getImageFile()).centerCrop().resize(70, 70).onlyScaleDown().into(profileImageview);
            }

            fullname.setText(alumni.getFullName());
            email.setText(alumni.getEmail());

        } catch (IllegalArgumentException ex) {
            Picasso.with(this).load(R.drawable.ic_emptyprofilepic).resize(70, 70).onlyScaleDown().into(profileImageview);
            fullname.setText(alumni.getFullName());
            email.setText(alumni.getEmail());
            Log.e(TAG, "retrieveRoleProfileDetails: Empty image path!");
        }


    }
}

package usmalumni.emerien.com.usmalumniapp.dashboard;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import usmalumni.emerien.com.usmalumniapp.model.Alumni;

/**
 * Created by Philip on 01/15/2018.
 */

public class DashboardPresenter implements DashboardView.ProfileDetails{

    public final static String TAG = DashboardPresenter.class.getSimpleName();
    DashboardView v;

    public DashboardPresenter(DashboardView view) {
        this.v=view;
    }


    @Override
    public void getProfileDetails(final String uuid) {
       Runnable r= new Runnable() {
           @Override
           public void run() {

               DatabaseReference ref= FirebaseDatabase.getInstance().getReference().child("Alumni").child(uuid);
               ref.addValueEventListener(new ValueEventListener() {
                   @Override
                   public void onDataChange(DataSnapshot dataSnapshot) {
                       Alumni alumni= dataSnapshot.getValue(Alumni.class);
                       v.retrieveRoleProfileDetails(alumni);
                       Log.e(TAG, "onDataChange: getProfileDetails "+dataSnapshot.getChildren().toString());
                   }

                   @Override
                   public void onCancelled(DatabaseError databaseError) {

                   }
               });

           }
       };

       Thread t= new Thread(r);
       t.start();
    }
}

package usmalumni.emerien.com.usmalumniapp.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import usmalumni.emerien.com.usmalumniapp.R;
import usmalumni.emerien.com.usmalumniapp.adapter.SearchUsersAdapter;
import usmalumni.emerien.com.usmalumniapp.model.Alumni;
import usmalumni.emerien.com.usmalumniapp.searchprofile.SearchUserRetrievePresenter;

/**
 * A simple {@link Fragment} subclass.
 */
public class UsersFragment extends Fragment implements SearchUserRetrievePresenter.GetAllUsersCallback,SearchUsersAdapter.OnAlumniSelectedListener{

    public final static String TAG = UsersFragment.class.getSimpleName();


    SearchUserRetrievePresenter searchUserRetrievePresenter;
    SearchUsersAdapter searchUsersAdapter;
    RecyclerView recyclerView;
    SearchView sv;
    View v;

    List<Alumni> alumniCollections= new ArrayList<>();

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchUserRetrievePresenter= new SearchUserRetrievePresenter();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_users, container, false);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView= (RecyclerView)v.findViewById(R.id.usersRecyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(v.getContext(),LinearLayoutManager.VERTICAL,false));
        sv= v.findViewById(R.id.userSearchview);
        sv.setActivated(true);
        sv.onActionViewExpanded();
        sv.setQueryHint("Search User");
    }

    @Override
    public void onResume() {
        super.onResume();
        searchUserRetrievePresenter.getAllUsers();
        searchUserRetrievePresenter.setGetAllUsersCallbackListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    /**
     * CALLBACK
     * @param alumniList
     */
    @Override
    public void getSingleAlumniData(List<Alumni> alumniList) {
        if (!alumniCollections.isEmpty()) {
            alumniCollections.clear();
        }

        alumniCollections=alumniList;
        searchUsersAdapter= new SearchUsersAdapter(alumniCollections);
        recyclerView.setAdapter(searchUsersAdapter);
        searchUsersAdapter.notifyDataSetChanged();
        searchUsersAdapter.setAlumniSelectedListener(this);
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                Log.e(TAG, "onQueryTextChange: TEXT CHANGED!" );

                if (s != null) {
                    searchUsersAdapter.getFilter().filter(s);
                    searchUsersAdapter.notifyDataSetChanged();
                }

                return false;
            }
        });
    }

    /**
     * ON ALUMNI SELECTED FROM LIST
     * @param alumni
     */
    @Override
    public void onAlumniSelected(Alumni alumni) {
        Log.e(TAG, "onAlumniSelected: "+alumni.getFullName() );
        displaySelectedAlumniProfile(alumni);
}

    private void displaySelectedAlumniProfile(Alumni alumni) {
        Bundle args= new Bundle();
        ViewSelectedFragment viewSelectedFragment= new ViewSelectedFragment();
        args.putString("uuid",alumni.getUuid());
        viewSelectedFragment.setArguments(args);

        FragmentTransaction ft= getFragmentManager().beginTransaction();
        ft.replace(R.id.selectedUserFragment,viewSelectedFragment,"TAG");
        ft.addToBackStack("displaySelected");
        ft.commit();
    }
}

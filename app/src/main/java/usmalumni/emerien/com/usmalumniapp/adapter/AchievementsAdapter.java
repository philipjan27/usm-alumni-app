package usmalumni.emerien.com.usmalumniapp.adapter;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import usmalumni.emerien.com.usmalumniapp.R;

/**
 * Created by Philip on 01/20/2018.
 */

public class AchievementsAdapter extends ArrayAdapter<String> {

    public final static String TAG = AchievementsAdapter.class.getSimpleName();

    List<String> achievementsList;
    Context ctx;
    SelectedAchievementCallback listener;


    private static class ViewHolder {
        TextView achivementDesc;
        ImageView deleteAchivement;
    }

    public AchievementsAdapter(@NonNull Context context, List<String> achievements) {
        super(context, R.layout.custom_achievement_layout,achievements);
        this.achievementsList=achievements;
        this.ctx=context;
    }

    @Override
    public int getCount() {
        Log.e(TAG, "getCount: SIZE: "+achievementsList.size() );
       return achievementsList.size();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(@Nullable String item) {
        return super.getPosition(item);
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            holder= new ViewHolder();
            LayoutInflater inflater= LayoutInflater.from(ctx);
            convertView= inflater.inflate(R.layout.custom_achievement_layout,parent,false);

            holder.achivementDesc= (TextView)convertView.findViewById(R.id.textview_achivement);
            holder.deleteAchivement= (ImageView)convertView.findViewById(R.id.imageview_delete);

            convertView.setTag(holder);
        }else {
            holder= (ViewHolder)convertView.getTag();
        }

        holder.achivementDesc.setText(achievementsList.get(position));
        holder.deleteAchivement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e(TAG, "onClick: Deleted Achivement: "+achievementsList.get(position)+" \n Position: "+position);
                achievementsList.remove(position);
                notifyDataSetChanged();

            }
        });

        return convertView;

    }

    public void setSelectedPositionListener(SelectedAchievementCallback cb) {
        this.listener=cb;
    }



    public interface SelectedAchievementCallback {
        void selectedItem(int pos);
    }


}

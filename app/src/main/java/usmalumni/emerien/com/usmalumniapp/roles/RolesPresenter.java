package usmalumni.emerien.com.usmalumniapp.roles;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import usmalumni.emerien.com.usmalumniapp.helpers.FirebaseConstants;
import usmalumni.emerien.com.usmalumniapp.model.Alumni;

/**
 * Created by Philip on 01/13/2018.
 */

public class RolesPresenter implements RolesView.CheckLoginRolesType{

    public final static String TAG = RolesPresenter.class.getSimpleName();
    RolesView rv;

    public RolesPresenter(RolesView rv) {
        this.rv=rv;
    }


    @Override
    public void checkLoginRole(final String email) {
        DatabaseReference reference= FirebaseDatabase.getInstance().getReference();
        reference.child("Alumni").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Alumni alumni= ds.getValue(Alumni.class);
                    if (email.equals(alumni.getEmail())) {
                        if (alumni.getRole().equals(FirebaseConstants.ADMIN_UUID)) {
                            rv.roleType("Admin",alumni.getUuid());
                            Log.e(TAG, "onDataChange: checkLoginRole -> Admin!");
                        }else if (alumni.getRole().equals(FirebaseConstants.ALUMNI_UUID)) {
                            rv.roleType("Alumni",alumni.getUuid());
                            Log.e(TAG, "onDataChange: checkLoginRole -> Alumni!");
                        }else {
                            rv.roleType("Faculty",alumni.getUuid());
                            Log.e(TAG, "onDataChange: checkLoginRole -> Faculty! "+alumni.getUuid());
                        }
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}

package usmalumni.emerien.com.usmalumniapp.addingannouncements;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import usmalumni.emerien.com.usmalumniapp.helpers.FirebaseUtilities;
import usmalumni.emerien.com.usmalumniapp.model.Announcements;

/**
 * Created by Philip on 02/01/2018.
 */

public class AnnouncementEditPresenter implements AnnouncementsView.EditAnnouncement {

    public final static String TAG = AnnouncementEditPresenter.class.getSimpleName();

    Context context;
    FirebaseUtilities utils;
    AnnouncementEditCallback callback;

    public AnnouncementEditPresenter(AnnouncementEditCallback cb, Context ctx) {
        this.callback = cb;
        utils = FirebaseUtilities.getInstance(ctx);
        this.context = ctx;
    }


    @Override
    public void editAnnouncement(final Announcements oldAnnouncements, final Announcements newAnnouncements, final Uri imageUri) {
        final StorageReference storageReference = FirebaseStorage.getInstance().getReference();
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Announcements").child(oldAnnouncements.getUuid());
        if (imageUri == null || imageUri.equals("")) {
//            Same image to use
            Log.e(TAG, "editAnnouncement: EDITING SELECTED ANNOUNCEMENTS WITHOUT CHANGING IMAGE FILE");
            saveNewDataToDb(ref, newAnnouncements);
        } else {
            StorageReference fileToDelete = storageReference.child("announcements/" + oldAnnouncements.getLocalImageFileName());
            fileToDelete.delete()
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.e(TAG, "onSuccess: Old image file successfully deleted");
                            StorageReference newFileToUpload = storageReference.child("announcements/" + oldAnnouncements.getUuid() + "." + utils.getImageExtension(imageUri, context));
                            newAnnouncements.setLocalImageFileName(oldAnnouncements.getUuid() + "." + utils.getImageExtension(imageUri, context));
                            newFileToUpload.putFile(imageUri)
                                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                            callback.imageReuploadSuccess();
                                            newAnnouncements.setImageFile(taskSnapshot.getDownloadUrl().toString());
//                          new image upload is success now save new data
                                            Log.i(TAG, "onSuccess: New image file successfully Uploaded");
                                            saveNewDataToDb(ref, newAnnouncements);
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            callback.imageReuploadFailed();
                                        }
                                    });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e(TAG, "onFailure: Old image file failed to delete. ERROR " + e.getLocalizedMessage());
                        }
                    });
        }

    }

    private void saveNewDataToDb(DatabaseReference ref, Announcements announcements) {
        ref.setValue(announcements)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        callback.onSuccessEditAnnouncement();
                        Log.i(TAG, "onSuccess: UPDATING CONTENT");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        callback.onFailureEditAnnouncement();
                        Log.i(TAG, "onFailure: UPDATING CONTENT");
                    }
                });
    }


    public interface AnnouncementEditCallback {
        void onSuccessEditAnnouncement();

        void onFailureEditAnnouncement();

        void imageReuploadSuccess();

        void imageReuploadFailed();
    }
}

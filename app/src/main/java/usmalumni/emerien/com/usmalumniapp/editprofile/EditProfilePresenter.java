package usmalumni.emerien.com.usmalumniapp.editprofile;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import usmalumni.emerien.com.usmalumniapp.model.Alumni;
import usmalumni.emerien.com.usmalumniapp.ownprofile.ProfileRetrievePresenter;

/**
 * Created by Philip on 03/06/2018.
 */

public class EditProfilePresenter implements EditProfileView.saveEditedProfile {

    DatabaseReference ref;
    ProfileUpdateCallback cb;

    public EditProfilePresenter() {

    }

    @Override
    public void saveNewEditedProfile(Alumni toBeEdited, Alumni newProfileUpdated) {
        ref = FirebaseDatabase.getInstance().getReference().child("Alumni").child(toBeEdited.getUuid());
        newProfileUpdated.setEmail(toBeEdited.getEmail());
        newProfileUpdated.setLocalImageFileName(toBeEdited.getLocalImageFileName());
        newProfileUpdated.setAwardsAndHonors(toBeEdited.getAwardsAndHonors());
        newProfileUpdated.setImageFile(toBeEdited.getImageFile());
        newProfileUpdated.setYearBatch(toBeEdited.getYearBatch());
        newProfileUpdated.setYearGraduated(toBeEdited.getYearGraduated());
        newProfileUpdated.setPassword(toBeEdited.getPassword());
        newProfileUpdated.setCourse(toBeEdited.getCourse());
        newProfileUpdated.setYearStartedSchooling(toBeEdited.getYearStartedSchooling());
        newProfileUpdated.setYearGraduated(toBeEdited.getYearGraduated());
        newProfileUpdated.setRole(toBeEdited.getRole());
        newProfileUpdated.setUuid(toBeEdited.getUuid());

        ref.setValue(newProfileUpdated)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        cb.onEditProfileSuccess();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        cb.onEditProfileFailed();
                    }
                });
    }

    public void setProfileUpdatecallbackListener(ProfileUpdateCallback callback) {
        this.cb=callback;
    }

    public interface ProfileUpdateCallback{
        void onEditProfileSuccess();
        void onEditProfileFailed();
    }
}

package usmalumni.emerien.com.usmalumniapp.roles;

import com.google.firebase.database.DataSnapshot;

import usmalumni.emerien.com.usmalumniapp.model.Alumni;

/**
 * Created by Philip on 01/13/2018.
 */

public interface RolesView {

    interface CheckLoginRolesType {
        void checkLoginRole(String email);
    }

    void roleType(String role, String userUUID);

}

package usmalumni.emerien.com.usmalumniapp.jobhirings;

import usmalumni.emerien.com.usmalumniapp.model.Jobs;

/**
 * Created by Philip on 01/23/2018.
 */

public interface JobHiringView {

    interface AddNewJobHiring {
        void addNewJobHiring(Jobs jobs);
    }

    interface RetrieveJobsList {
        void getJobsList();
    }

    interface DeleteJobHiring {
        void deleteJobHiring(String uuid);
    }

    interface EditJobHiring {
        void editJobHiring(Jobs selectedJobs,Jobs newCreatedJob);
    }

    void addJobSuccess();
    void addJobFailure();
}

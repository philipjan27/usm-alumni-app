package usmalumni.emerien.com.usmalumniapp.model;

/**
 * Created by Philip on 01/04/2018.
 */

public class Jobs {

    private String uuid;
    private String jobPositionName;
    private String jobPositionDescription;
    private Long datePostedTimeStamp;


    public Jobs(String uuid, String jobPositionName, String jobPositionDescription, Long datePostedTimeStamp) {
        this.uuid = uuid;
        this.jobPositionName = jobPositionName;
        this.jobPositionDescription = jobPositionDescription;
        this.datePostedTimeStamp = datePostedTimeStamp;
    }

    public Jobs() {
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getJobPositionName() {
        return jobPositionName;
    }

    public void setJobPositionName(String jobPositionName) {
        this.jobPositionName = jobPositionName;
    }

    public String getJobPositionDescription() {
        return jobPositionDescription;
    }

    public void setJobPositionDescription(String jobPositionDescription) {
        this.jobPositionDescription = jobPositionDescription;
    }

    public Long getDatePostedTimeStamp() {
        return datePostedTimeStamp;
    }

    public void setDatePostedTimeStamp(Long datePostedTimeStamp) {
        this.datePostedTimeStamp = datePostedTimeStamp;
    }
}

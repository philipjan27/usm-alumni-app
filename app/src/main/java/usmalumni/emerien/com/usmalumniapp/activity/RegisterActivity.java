package usmalumni.emerien.com.usmalumniapp.activity;


import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.SyncStateContract;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;


import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import usmalumni.emerien.com.usmalumniapp.R;
import usmalumni.emerien.com.usmalumniapp.adapter.AchievementsAdapter;
import usmalumni.emerien.com.usmalumniapp.helpers.FirebaseConstants;
import usmalumni.emerien.com.usmalumniapp.helpers.FirebaseUtilities;
import usmalumni.emerien.com.usmalumniapp.model.Alumni;


public class RegisterActivity extends AppCompatActivity implements FirebaseUtilities.RegistrationListener, AchievementsAdapter.SelectedAchievementCallback {

    public final static String TAG = RegisterActivity.class.getSimpleName();
    StorageReference storageRootReference;

    String civilStatusValue="";
    String workFieldValue="";
    String roleUUID="";
    Uri imageUri;
    FirebaseUtilities utils;
    Alumni alumni;
    String started, graduated = "";
    String[] years;
    ArrayAdapter<String> adapterStarted, adapterGraduated,civilStatusAdapter,workFieldAdapter;
    List<String> listyears= new ArrayList<>();
    AchievementsAdapter expLVAdapter;

    ExpandableHeightListView expandableHeightListView;
    MaterialSpinner spinnerStarted, spinnerGraduated,civilStatus,workfield;
    Toolbar toolbar;
    TextInputEditText fullName, email, password, course, address,mobileNumber,jobPosition,companyName,achievements;
    ImageView profileImage,achievementAdd;
    RadioGroup rg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        storageRootReference = FirebaseStorage.getInstance().getReference();

        initToolbar();
        initializeWidgets();
        setWidgetListener();
        utils = FirebaseUtilities.getInstance(RegisterActivity.this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        utils.setRegistrationCallbackListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 90 && resultCode == RESULT_OK && data != null && data.getData() != null) {

            if (data != null) {
                imageUri = data.getData();
            }else {
                Log.e(TAG, "onActivityResult: NULL IMAGE URI!");

            }


            try {
                Bitmap bmp = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                profileImage.setImageBitmap(bmp);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void initToolbar() {
        toolbar = findViewById(R.id.registerToolbar);
        toolbar.setTitle("Fill out needed details");
        toolbar.setTitleTextColor(getResources().getColor(R.color.accent));
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_register:
                registerUser();
                break;
        }

        return false;

    }

    private void initializeWidgets() {
        spinnerStarted = findViewById(R.id.spinner_yearStarted);
        spinnerGraduated = findViewById(R.id.spinner_yearGraduated);
        fullName = findViewById(R.id.register_fullname);
        email = findViewById(R.id.register_email);
        password = findViewById(R.id.register_password);
        course = findViewById(R.id.register_course);
        address = findViewById(R.id.register_address);
        profileImage = findViewById(R.id.imageview_profilepicture_preview);
        rg = findViewById(R.id.radio_group);
        expandableHeightListView= findViewById(R.id.register_expandable_listview);
        civilStatus= findViewById(R.id.spinner_civilStatus);
        workfield= findViewById(R.id.spinner_workField);
        mobileNumber= findViewById(R.id.register_mobilenumber);
        jobPosition= findViewById(R.id.register_jobPosition);
        companyName=findViewById(R.id.register_companyName);
        achievements=findViewById(R.id.register_achievements);
        achievementAdd= findViewById(R.id.imageview_add_achievement);
    }

    private void setWidgetListener() {



        adapterStarted = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, generateYears());
        adapterGraduated = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, generateYears());
        spinnerStarted.setAdapter(adapterStarted);
        spinnerGraduated.setAdapter(adapterGraduated);

        civilStatusAdapter= new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.civil_status));
        civilStatus.setAdapter(civilStatusAdapter);

        workFieldAdapter= new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.workfield));
        workfield.setAdapter(workFieldAdapter);



        civilStatus.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                civilStatusValue= (String)item.toString();
            }
        });

            workfield.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                    workFieldValue= item.toString();
                }
            });


        spinnerStarted.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                Log.e(TAG, "onItemSelected: Started Since " + item.toString());
                started = item.toString();
            }
        });


        spinnerGraduated.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                Log.e(TAG, "onItemSelected: Graduated at Year " + item.toString());
                graduated = item.toString();

            }
        });

//        get selected image from the image picker intent
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), 90);
            }
        });

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton rb = (RadioButton) findViewById(i);

                if (rb.isChecked()) {
                    if (rb.getText().toString().equals("Alumni")) {
                        roleUUID= FirebaseConstants.ALUMNI_UUID;
                        Log.e(TAG, "onCheckedChanged: " + FirebaseConstants.ALUMNI_UUID);
                    } else if (rb.getText().toString().equals("Faculty")){
                        roleUUID= FirebaseConstants.FACULTY_UUID;
                        Log.e(TAG, "onCheckedChanged: " + FirebaseConstants.FACULTY_UUID);
                    }
                }

                Log.e(TAG, "onCheckedChanged: ROLE: "+roleUUID );

            }
        });

        expLVAdapter= new AchievementsAdapter(this,listyears);
        expandableHeightListView.setAdapter(expLVAdapter);
        expandableHeightListView.setExpanded(true);
        expLVAdapter.setSelectedPositionListener(this);
        expLVAdapter.notifyDataSetChanged();

        expandableHeightListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e(TAG, "onItemClick: "+adapterView.getAdapter().getItem(i).toString() );
            }
        });

        achievementAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (achievements.getText().toString().isEmpty()) {
                    achievements.setError("Empty values");
                }else {
                    listyears.add(achievements.getText().toString());
                    achievements.setText("");
                }
                    updateListview();
            }
        });
    }

    private void updateListview() {
        expLVAdapter= new AchievementsAdapter(this,listyears);
        expandableHeightListView.setAdapter(expLVAdapter);
        expLVAdapter.notifyDataSetChanged();
    }

    private void registerUser() {

        if (imageUri == null) {
            imageUri=Uri.parse("android.resource://usmalumni.emerien.com.usmalumniapp/drawable/ic_emptyprofilepic");
        }

        if (started.isEmpty() || graduated.isEmpty()) {
            started="1950";
            graduated="1950";
        }

        alumni = new Alumni();
        alumni.setAddress(address.getText().toString());
        alumni.setCourse(course.getText().toString());
        alumni.setEmail(email.getText().toString());
        alumni.setFullName(fullName.getText().toString());
        alumni.setPassword(password.getText().toString());
        alumni.setRole(roleUUID);
        alumni.setMobileNumber(mobileNumber.getText().toString());
        alumni.setCurrentJob(workFieldValue);
        alumni.setJobPosition(jobPosition.getText().toString());
        alumni.setCompanyName(companyName.getText().toString());
        alumni.setYearStartedSchooling(Integer.parseInt(started));
        alumni.setYearGraduated(Integer.parseInt(graduated));
        alumni.setYearBatch(String.valueOf(alumni.getYearStartedSchooling()) + "-" + String.valueOf(alumni.getYearGraduated()));
        alumni.setCivilStatus(civilStatusValue);
        alumni.setAwardsAndHonors(listyears);

        utils.addNewAlumni(alumni, imageUri, storageRootReference, RegisterActivity.this);
        Log.d(TAG, "registerUser: Register Clicked!");
    }

    @Override
    public void onSuccess() {
        Log.e(TAG, "onSuccess: ");
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void onFail() {
        Log.e(TAG, "onFail: ");
    }

    public String [] generateYears() {
        int startYear=1950;
        String [] years=  new String[100];
        for (int i=0; i<years.length; i++) {
            years[i]=String.valueOf(startYear++);
            Log.e(TAG, "generateYears: years["+i+"] : "+String.valueOf(years[i]));
        }
        return years;
    }

    @Override
    public void selectedItem(int pos) {

    }

}

package usmalumni.emerien.com.usmalumniapp.fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import usmalumni.emerien.com.usmalumniapp.R;
import usmalumni.emerien.com.usmalumniapp.adapter.JobHiringsRecyclerViewAdapter;
import usmalumni.emerien.com.usmalumniapp.helpers.AppUtils;
import usmalumni.emerien.com.usmalumniapp.helpers.SharedPreferenceHelper;
import usmalumni.emerien.com.usmalumniapp.jobhirings.AddJobHiringPresenter;
import usmalumni.emerien.com.usmalumniapp.jobhirings.EditJobHiringPresenter;
import usmalumni.emerien.com.usmalumniapp.jobhirings.JobHiringView;
import usmalumni.emerien.com.usmalumniapp.model.Jobs;

/**
 * A simple {@link Fragment} subclass.
 */
public class JobHiringFragment extends Fragment implements JobHiringView, JobHiringsRecyclerViewAdapter.ItemClickedCallack, EditJobHiringPresenter.JobHiringEditCallback {

    public final static String TAG = JobHiringFragment.class.getSimpleName();

    EditJobHiringPresenter editPresenter;
    AddJobHiringPresenter addJobHiringPresenter;
    JobHiringsRecyclerViewAdapter adapter;
    SharedPreferenceHelper prefhelper;
    SharedPreferences prefs;
    Jobs jobsToBeAdded,referenceToBeEdited;

    FloatingActionButton fab;
    RecyclerView jobHiringRv;
    ProgressDialog dialog;
    View v;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefhelper = SharedPreferenceHelper.getInstance();
        prefs = prefhelper.initializePrefs(getActivity());

        addJobHiringPresenter = new AddJobHiringPresenter(this);
        editPresenter= new EditJobHiringPresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_job_hiring, container, false);

        jobHiringRv = v.findViewById(R.id.jobhiring_recycleview);
        fab = v.findViewById(R.id.jobhiring_fab);
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Publishing Job Hiring to the server..");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);


//      For the recycleview to display new values to the top instead of bottom
        LinearLayoutManager llm = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        llm.setReverseLayout(true);
        llm.setStackFromEnd(true);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialog(jobsToBeAdded,"Add Job Hiring", "add");
            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        llm.setReverseLayout(true);
        llm.setStackFromEnd(true);

        jobHiringRv.setLayoutManager(llm);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!prefhelper.getCurrentRoleInPrefs(prefs).equals("Admin")) {
            fab.setVisibility(View.GONE);
            adapter = new JobHiringsRecyclerViewAdapter(false, this);
        } else {
            fab.setVisibility(View.VISIBLE);
            adapter = new JobHiringsRecyclerViewAdapter(true, this);
        }

        jobHiringRv.setAdapter(adapter);
    }


    private void createDialog(final Jobs tobeEdit, String titleText, String method) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        View v = getActivity().getLayoutInflater().inflate(R.layout.custom_addjobhiring_dialog, null);
        final TextView jobPos = v.findViewById(R.id.editext_position);
        final TextView shortDesc = v.findViewById(R.id.editext_shortdesck);
        builder.setView(v);
        builder.setTitle(titleText);
        builder.setInverseBackgroundForced(true);

           if (method.equals("add")) {

               builder.setPositiveButton("PUBLISH", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialogInterface, int i) {
//`             PRESENTER TO ADD NEW JOB HIRINGS

                       jobsToBeAdded = new Jobs();
                       jobsToBeAdded.setDatePostedTimeStamp(System.currentTimeMillis());
                       jobsToBeAdded.setJobPositionDescription(shortDesc.getText().toString());
                       jobsToBeAdded.setJobPositionName(jobPos.getText().toString());

                      if (AppUtils.isDeviceConnectedToAnyNetworks(getActivity())){
                          addJobHiringPresenter.addNewJobHiring(jobsToBeAdded);
                          dialog.show();
                      }else {
                          Toasty.error(getActivity(),"No internet connection. Please try again",Toast.LENGTH_SHORT,true).show();
                      }
                   }
               }).setNegativeButton("DISMISS", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialogInterface, int i) {

                   }
               });

           } else if (method.equals("edit")) {
               jobPos.setText(tobeEdit.getJobPositionName());
               shortDesc.setText(tobeEdit.getJobPositionDescription());

               builder.setPositiveButton("PUBLISH", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialogInterface, int i) {
//`             PRESENTER TO ADD NEW JOB HIRINGS

                       referenceToBeEdited = new Jobs();
                       referenceToBeEdited.setDatePostedTimeStamp(System.currentTimeMillis());
                       referenceToBeEdited.setJobPositionDescription(shortDesc.getText().toString());
                       referenceToBeEdited.setJobPositionName(jobPos.getText().toString());
                       referenceToBeEdited.setUuid(tobeEdit.getUuid());

                      if (AppUtils.isDeviceConnectedToAnyNetworks(getActivity())){
                          editPresenter.editJobHiring(tobeEdit,referenceToBeEdited);
                          dialog.show();
                      }else {
                          Toasty.error(getActivity(),"No internet connection. Please try again",Toast.LENGTH_SHORT,true).show();
                      }
                   }
               }).setNegativeButton("DISMISS", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialogInterface, int i) {

                   }
               });

           }



        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.show();

    }


    /**
     * CALLBACK OF JOB HIRING WHEN EDIT/DELETE IS CLICKED
     *
     * @param job
     */
    @Override
    public void onEditClicked(Jobs job) {
        createDialog(job,"Edit Job Details","edit");
    }

    @Override
    public void onDeleteClicked(Jobs job) {

    }


    /**
     * ADDING NEW JOB CALLBACK
     */
    @Override
    public void addJobSuccess() {
        dialog.dismiss();
        Toasty.success(getActivity(), "Add Job Hiring success", Toast.LENGTH_LONG, true).show();
    }

    @Override
    public void addJobFailure() {
        dialog.dismiss();
        Toasty.error(getActivity(), "Error in adding new Job Hiring", Toast.LENGTH_LONG, true).show();
    }

    /**
     * EDIT JOB HIRING CALLBACK
     */
    @Override
    public void successJobHiringEdit() {
        dialog.dismiss();
        Toasty.success(getActivity(),"Success editing Job details",Toast.LENGTH_LONG,true).show();
    }

    @Override
    public void failedJobHiringEdit() {
        dialog.dismiss();
        Toasty.error(getActivity(),"Error editing Job details",Toast.LENGTH_LONG,true).show();
    }
}

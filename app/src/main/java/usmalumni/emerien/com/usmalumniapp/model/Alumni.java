package usmalumni.emerien.com.usmalumniapp.model;

import java.util.List;

/**
 * Created by Philip on 01/04/2018.
 */

public class Alumni {

    private String fullName;
    private String email;
    private String password;
    private String course;
    private String role;
    private String uuid;
    private String address;
    private int yearStartedSchooling;
    private int yearGraduated;
    private String yearBatch;
    private String imageFile;
    private String mobileNumber;
    private String civilStatus;
    private String currentJob;
    private String jobPosition;
    private String companyName;
    private List<String> awardsAndHonors;
    private String localImageFileName;

    public Alumni(String fullName, String email, String password, String course, String role, String uuid, String address, int yearStartedSchooling, int yearGraduated, String yearBatch, String imageFile, String mobileNumber, String civilStatus, String currentJob, String jobPosition, String companyName, List<String> awardsAndHonors,String localImageFileName) {
        this.fullName = fullName;
        this.email = email;
        this.password = password;
        this.course = course;
        this.role = role;
        this.uuid = uuid;
        this.address = address;
        this.yearStartedSchooling = yearStartedSchooling;
        this.yearGraduated = yearGraduated;
        this.yearBatch = yearBatch;
        this.imageFile = imageFile;
        this.mobileNumber = mobileNumber;
        this.civilStatus = civilStatus;
        this.currentJob = currentJob;
        this.jobPosition = jobPosition;
        this.companyName = companyName;
        this.awardsAndHonors = awardsAndHonors;
        this.localImageFileName=localImageFileName;
    }

    public Alumni() {
    }

    public Alumni(String fullName, String email, String password, String course, String uuid, String address, int yearStartedSchooling, int yearGraduated, String yearBatch, String imageFile, String mobileNumber, String civilStatus, String currentJob, String jobPosition, String companyName, List<String> awardsAndHonors) {
        this.fullName = fullName;
        this.email = email;
        this.password = password;
        this.course = course;
        this.uuid = uuid;
        this.address = address;
        this.yearStartedSchooling = yearStartedSchooling;
        this.yearGraduated = yearGraduated;
        this.yearBatch = yearBatch;
        this.imageFile = imageFile;
        this.mobileNumber = mobileNumber;
        this.civilStatus = civilStatus;
        this.currentJob = currentJob;
        this.jobPosition = jobPosition;
        this.companyName = companyName;
        this.awardsAndHonors = awardsAndHonors;
    }

    public Alumni(String fullName, String email, String password, String course, String uuid, String address, int yearStartedSchooling, int yearGraduated, String yearBatch, String imageFile, String mobileNumber, String civilStatus, String currentJob, String jobPosition, String companyName) {
        this.fullName = fullName;
        this.email = email;
        this.password = password;
        this.course = course;
        this.uuid = uuid;
        this.address = address;
        this.yearStartedSchooling = yearStartedSchooling;
        this.yearGraduated = yearGraduated;
        this.yearBatch = yearBatch;
        this.imageFile = imageFile;
        this.mobileNumber = mobileNumber;
        this.civilStatus = civilStatus;
        this.currentJob = currentJob;
        this.jobPosition = jobPosition;
        this.companyName = companyName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getYearStartedSchooling() {
        return yearStartedSchooling;
    }

    public void setYearStartedSchooling(int yearStartedSchooling) {
        this.yearStartedSchooling = yearStartedSchooling;
    }

    public int getYearGraduated() {
        return yearGraduated;
    }

    public void setYearGraduated(int yearGraduated) {
        this.yearGraduated = yearGraduated;
    }

    public String getYearBatch() {
        return yearBatch;
    }

    public void setYearBatch(String yearBatch) {
        this.yearBatch = yearBatch;
    }

    public String getImageFile() {
        return imageFile;
    }

    public void setImageFile(String imageFile) {
        this.imageFile = imageFile;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getCivilStatus() {
        return civilStatus;
    }

    public void setCivilStatus(String civilStatus) {
        this.civilStatus = civilStatus;
    }

    public String getCurrentJob() {
        return currentJob;
    }

    public void setCurrentJob(String currentJob) {
        this.currentJob = currentJob;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public List<String> getAwardsAndHonors() {
        return awardsAndHonors;
    }

    public void setAwardsAndHonors(List<String> awardsAndHonors) {
        this.awardsAndHonors = awardsAndHonors;
    }

    public String getLocalImageFileName() {
        return localImageFileName;
    }

    public void setLocalImageFileName(String localImageFileName) {
        this.localImageFileName = localImageFileName;
    }
}
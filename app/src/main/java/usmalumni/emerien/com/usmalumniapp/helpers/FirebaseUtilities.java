package usmalumni.emerien.com.usmalumniapp.helpers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;


import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import es.dmoral.toasty.Toasty;
import usmalumni.emerien.com.usmalumniapp.R;
import usmalumni.emerien.com.usmalumniapp.model.Achievements;
import usmalumni.emerien.com.usmalumniapp.model.Alumni;
import usmalumni.emerien.com.usmalumniapp.model.Roles;
import usmalumni.emerien.com.usmalumniapp.ownprofile.AchievementsTransactionPresenter;

/**
 * Created by Philip on 01/09/2018.
 */

public class FirebaseUtilities {

    RegistrationListener listener;


    StorageReference storageReference;
    Uri imageUriToBeUploaded;

    public final static String TAG = FirebaseUtilities.class.getSimpleName();
    ProgressDialog indicatorDialog;
    FirebaseAuth regAuth;
    DatabaseReference ref, refRoles, refAlumni, refAchievements;
    StorageReference alumniStorageReferances;
    Alumni alumni;
    public static Context ctx;
    private static FirebaseUtilities instance = null;

    public static FirebaseUtilities getInstance(Context context) {
        if (instance == null) {
            instance = new FirebaseUtilities();
            ctx = context;
        }
        return instance;
    }


    public void checkIfNeededTableExist() {

        Runnable r = new Runnable() {
            @Override
            public void run() {

                Log.e(TAG, "run: Running inside Runnable!");
                ref = FirebaseDatabase.getInstance().getReference();
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChild("Roles")) {
                            addRolesTable(ref);
                        } else {
                            getSnapshots();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        };

        Thread t = new Thread(r);
        t.start();

    }


    private void getSnapshots() {

        Runnable r = new Runnable() {
            @Override
            public void run() {

                ref.child("Roles").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Roles roles = snapshot.getValue(Roles.class);

                            switch (roles.getRole()) {
                                case "Admin":
                                    FirebaseConstants.ADMIN_UUID = roles.getUuid();
                                    break;
                                case "Faculty":
                                    FirebaseConstants.FACULTY_UUID = roles.getUuid();
                                    break;
                                case "Alumni":
                                    FirebaseConstants.ALUMNI_UUID = roles.getUuid();
                            }

                            Log.e(TAG, "onDataChange: UUID " + roles.getUuid());
                        }

                        addAdminCredentialsToFirebaseDb();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }
        };

        Thread t = new Thread(r);
        t.start();


    }

    private void addRolesTable(final DatabaseReference ref) {

        Runnable r = new Runnable() {
            @Override
            public void run() {

                refRoles = ref.child("Roles");

                Roles admin = new Roles();
                Roles faculty = new Roles();
                Roles alumni = new Roles();

                admin.setUuid(refRoles.push().getKey());
                admin.setRole("Admin");
                refRoles.child(admin.getUuid()).setValue(admin).addOnFailureListener(realtimeDBRoles);

                faculty.setUuid(refRoles.push().getKey());
                faculty.setRole("Faculty");
                refRoles.child(faculty.getUuid()).setValue(faculty).addOnFailureListener(realtimeDBRoles);

                alumni.setUuid(refRoles.push().getKey());
                alumni.setRole("Alumni");
                refRoles.child(alumni.getUuid()).setValue(alumni).addOnFailureListener(realtimeDBRoles);


                Log.e(TAG, "addRolesTable: Admin / Faculty / Alumni " + FirebaseConstants.ADMIN_UUID + "\n" + FirebaseConstants.FACULTY_UUID + "\n" + FirebaseConstants.ALUMNI_UUID);


            }
        };

        Thread t = new Thread(r);
        t.start();

    }

    public void checkAdminCredentialsInFirebase() {
        ref = FirebaseDatabase.getInstance().getReference();
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChild("Alumni")) {
                    addAdminCredentialsToFirebaseDb();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void addAdminCredentialsToFirebaseDb() {


        Runnable r = new Runnable() {
            @Override
            public void run() {

                final String adminUUID = "-@dM1N";

                final Alumni adminProfile = new Alumni();
                adminProfile.setFullName(ctx.getResources().getString(R.string.admin_fullname));
                adminProfile.setEmail(ctx.getResources().getString(R.string.admin_email));
                adminProfile.setPassword(ctx.getResources().getString(R.string.admin_password));
                adminProfile.setCourse("BSCS");
                adminProfile.setRole(FirebaseConstants.ADMIN_UUID);
                adminProfile.setUuid(adminUUID);
                adminProfile.setAddress("Kabacan Cotabato");
                adminProfile.setYearStartedSchooling(1990);
                adminProfile.setYearGraduated(1995);
                adminProfile.setYearBatch("1990 - 1995");
                adminProfile.setImageFile("");
                adminProfile.setMobileNumber("09482914265");
                adminProfile.setCivilStatus("Single");
                adminProfile.setCurrentJob("IT(Software/Hardware)");
                adminProfile.setJobPosition("System Administrator");
                adminProfile.setCompanyName("Genius Corp");


                final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
                firebaseAuth.signInWithEmailAndPassword(ctx.getResources().getString(R.string.admin_email), ctx.getResources().getString(R.string.admin_password))
                        .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                            @Override
                            public void onSuccess(AuthResult authResult) {
                                Log.e(TAG, "onSuccess: Able to signin, no need to create new data!");
                                //   do not re create account!
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.e(TAG, "onFailure: FAILED SIGNING IN!");
                                createAdministratorAccount(adminUUID,adminProfile);

                            }
                        });
            }
        };
        new Thread(r).start();
    }

    private void createAdministratorAccount(final String adminUUID, final Alumni adminProfile){
        FirebaseAuth firebaseAuth= FirebaseAuth.getInstance();
        firebaseAuth.createUserWithEmailAndPassword(ctx.getResources().getString(R.string.admin_email), ctx.getResources().getString(R.string.admin_password))
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        Log.e(TAG, "onSuccess: SUCCESS CREATING ADMIN ACCOUNT!");

                        //  create admin account
                        ref.child("Alumni").child(adminUUID).setValue(adminProfile).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.e(TAG, "onSuccess: SUCCESS CREATING ADMIN REALTIME DB DATA");
                                addAdminAchievements();
                            }
                        });


                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "onFailure: ALREADY HAVE THIS CREDENTIAL!");
                    }
                });
    }

    private void addAdminAchievements() {
        AchievementsTransactionPresenter achievementsTransactionPresenter = new AchievementsTransactionPresenter();
        Achievements singleAchievement = new Achievements();
        singleAchievement.setUserUUID("-@dM1N");
        singleAchievement.setAchievementDetails("Default Achievement,please add/edit/delete in your profile");
        achievementsTransactionPresenter.addNewAchievement(singleAchievement);
        achievementsTransactionPresenter.setAchievementAddCallbackListener(new AchievementsTransactionPresenter.AchievementAddCallback() {
            @Override
            public void onSuccessAdd() {
                Log.e(TAG, "onSuccessAdd: SUCCESS ADDING ADMIN DEFAULT ACHIEVEMENT");
            }

            @Override
            public void onFailureAdd() {
                Log.e(TAG, "onFailureAdd: SUCCESS ADDING ADMIN DEFAULT ACHIEVEMENT");
            }
        });
    }

    public void addNewAlumni(Alumni obj, Uri imageUri, StorageReference storageRootReference, Activity act) {

        storageReference = storageRootReference;
        imageUriToBeUploaded = imageUri;

        indicatorDialog = new ProgressDialog(act);
        indicatorDialog.setTitle("Registration");
        indicatorDialog.setMessage("Submitting data. Please wait");
        indicatorDialog.setCanceledOnTouchOutside(false);
        indicatorDialog.setCancelable(false);
        indicatorDialog.show();

        ctx = act;
        regAuth = FirebaseAuth.getInstance();
        refAlumni = ref.child("Alumni");

        alumni = obj;
        alumni.setUuid(refAlumni.push().getKey());

//      Register to the Firebase Auth Function
        regAuth.createUserWithEmailAndPassword(alumni.getEmail(), alumni.getPassword())
                .addOnCompleteListener(profileAuthRegistrationListener)
                .addOnFailureListener(failureListener);
    }


    public String getImageExtension(Uri uri, Context ctx) {
        ContentResolver resolver = ctx.getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        String fileExt = mimeTypeMap.getExtensionFromMimeType(resolver.getType(uri));
        Log.e(TAG, "getImageExtension: " + fileExt);
        return mimeTypeMap.getExtensionFromMimeType(resolver.getType(uri));
    }


    //    Profile image upload success listener
    OnSuccessListener<UploadTask.TaskSnapshot> profileImageUploadSuccessListener = new OnSuccessListener<UploadTask.TaskSnapshot>() {
        @Override
        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
            saveDataToRealtimeDb(taskSnapshot);
        }
    };


    //    Authentication Registration listeners
    OnCompleteListener<AuthResult> profileAuthRegistrationListener = new OnCompleteListener<AuthResult>() {
        @Override
        public void onComplete(@NonNull Task<AuthResult> task) {
            if (task.isSuccessful()) {
//                indicatorDialog.dismiss();
                uploadImage();
            }
        }
    };

    OnFailureListener failureListener = new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
            indicatorDialog.dismiss();
            Toasty.error(ctx, "An error occured. Please try again", Toast.LENGTH_LONG, true).show();
            Log.e(TAG, "onFailure: failureListener" + e.getMessage());
            listener.onFail();
        }
    };


    //    Realtime database success listener
    OnSuccessListener<Void> alumniDatabaseSuccessListener = new OnSuccessListener<Void>() {
        @Override
        public void onSuccess(Void aVoid) {
            refAchievements = ref.child("Achievements");
            for (int i = 0; i < alumni.getAwardsAndHonors().size(); i++) {

                Achievements achievements = new Achievements();
                achievements.setAchievementDetails(alumni.getAwardsAndHonors().get(i));
                achievements.setUserUUID(alumni.getUuid());
                achievements.setUuid(refAchievements.push().getKey());

                refAchievements.child(achievements.getUuid()).setValue(achievements).addOnSuccessListener(achievementsToDbSuccessListener);
            }

            indicatorDialog.dismiss();
            listener.onSuccess();

        }
    };

    OnFailureListener realtimeDBRoles = new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
            Log.e(TAG, "onFailure: " + e.getMessage());
        }
    };

    public void setRegistrationCallbackListener(RegistrationListener listener) {
        this.listener = listener;
    }

    OnSuccessListener<Void> achievementsToDbSuccessListener = new OnSuccessListener<Void>() {
        @Override
        public void onSuccess(Void aVoid) {
            /*indicatorDialog.dismiss();
            listener.onSuccess();*/
        }
    };

    public interface RegistrationListener {
        void onSuccess();

        void onFail();
    }

    //    Upload image to the firebase storage
    private void uploadImage() {
        alumniStorageReferances = storageReference.child("alumni/" + alumni.getUuid() + "." + getImageExtension(imageUriToBeUploaded, ctx));
        alumniStorageReferances.putFile(imageUriToBeUploaded)
                .addOnSuccessListener(profileImageUploadSuccessListener)
                .addOnFailureListener(failureListener);
    }

    //    Save Data to the Realtime database
    private void saveDataToRealtimeDb(UploadTask.TaskSnapshot snap) {
        alumni.setImageFile(snap.getDownloadUrl().toString());
        alumni.setLocalImageFileName(alumni.getUuid() + "." + getImageExtension(imageUriToBeUploaded, ctx));
        refAlumni.child(alumni.getUuid()).setValue(alumni)
                .addOnSuccessListener(alumniDatabaseSuccessListener)
                .addOnFailureListener(failureListener);
    }


}

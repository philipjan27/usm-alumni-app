package usmalumni.emerien.com.usmalumniapp.fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.google.firebase.database.DatabaseError;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.dmoral.toasty.Toasty;
import usmalumni.emerien.com.usmalumniapp.R;
import usmalumni.emerien.com.usmalumniapp.adapter.AchievementsProfileAdapter;
import usmalumni.emerien.com.usmalumniapp.editprofile.EditProfilePresenter;
import usmalumni.emerien.com.usmalumniapp.helpers.AppUtils;
import usmalumni.emerien.com.usmalumniapp.helpers.SharedPreferenceHelper;
import usmalumni.emerien.com.usmalumniapp.model.Achievements;
import usmalumni.emerien.com.usmalumniapp.model.Alumni;
import usmalumni.emerien.com.usmalumniapp.ownprofile.AchievementsTransactionPresenter;
import usmalumni.emerien.com.usmalumniapp.ownprofile.ProfileRetrievePresenter;


public class EditProfileFragment extends Fragment implements View.OnClickListener, ProfileRetrievePresenter.RetrieveProfileCallback
        ,EditProfilePresenter.ProfileUpdateCallback
        ,ProfileRetrievePresenter.RetrieveAchievementsCallbackMethod2
        , AchievementsProfileAdapter.OnAchievementEditOrDeleteCallbackListener
        ,AchievementsTransactionPresenter.AchievementEditCallback
        ,AchievementsTransactionPresenter.AchievementDeleteCallback
        ,AchievementsTransactionPresenter.AchievementAddCallback{

    public final static String TAG = EditProfileFragment.class.getSimpleName();

    SharedPreferenceHelper helper;
    SharedPreferences prefs;
    EditProfilePresenter editProfilePresenter;
    ProfileRetrievePresenter retrievePresenter;
    String[] civilStatusList;
    String[] workFieldList;
    String selectedCivilStatus,selectedWorkField;
    String userUUID;
    Alumni currentUserData;
    AchievementsProfileAdapter achievementsAdapter;
    List<Achievements> achievementsList= new ArrayList<>();
    Achievements newAchievement;
    AchievementsTransactionPresenter achievementsTransactionPresenter;



    View v;
    TextInputEditText name,mobileNumber,address,jobPosition,companyName,addAchievements;
    ImageView addAchievementsImg;
    ExpandableHeightListView expandableHeightListView;
    Button saveData;
    MaterialSpinner civilStatus,workField;
    MaterialSpinnerAdapter civilStatusadapter, workfieldAdapter;
    ProgressDialog pd;


    @Override
    public void onStart() {
        super.onStart();
        Log.e(TAG, "onStart: EditProfileFragment" );
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate: EditProfileFragment");
        super.onCreate(savedInstanceState);
        editProfilePresenter= new EditProfilePresenter();
        retrievePresenter= new ProfileRetrievePresenter(this);
        achievementsTransactionPresenter= new AchievementsTransactionPresenter();
        helper= SharedPreferenceHelper.getInstance();
        prefs=helper.initializePrefs(getActivity());
        userUUID= helper.getUserUUIDInPrefs(prefs);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.e(TAG, "onViewCreated: EditProfileFragment" );
        super.onViewCreated(view, savedInstanceState);
        initViews();
        setAdapters();
        setSpinnerListeners();
        retrievePresenter.getUserProfile(userUUID);
        retrievePresenter.getUserAchievementsAnotherMethod(userUUID);
        retrievePresenter.setRetrieveAchievementsMethod2Listener(this);
        achievementsTransactionPresenter.setAchievementEditCallbackListeners(this);
        achievementsTransactionPresenter.setAchievementDeleteCallbackListener(this);
        achievementsTransactionPresenter.setAchievementAddCallbackListener(this);

    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume: EditProfileFragment" );
        super.onResume();
    }

    public void initViews() {
        name= (TextInputEditText)getActivity().findViewById(R.id.editprofile_fullname);
        mobileNumber= (TextInputEditText) getActivity().findViewById(R.id.editprofile_mobilenumber);
        address=(TextInputEditText)getActivity().findViewById(R.id.editprofile_address);
        jobPosition= (TextInputEditText) getActivity().findViewById(R.id.editprofile_jobPosition);
        companyName= (TextInputEditText)getActivity().findViewById(R.id.editprofile_companyName);
        saveData= (Button) getActivity().findViewById(R.id.editprofile_saveData);
        civilStatus= (MaterialSpinner) getActivity().findViewById(R.id.editprofile_civilstatus);
        workField= (MaterialSpinner) getActivity().findViewById(R.id.editprofile_workfield);
        expandableHeightListView= (ExpandableHeightListView) getActivity().findViewById(R.id.editprofile_expandableListView);
        addAchievements= (TextInputEditText) getActivity().findViewById(R.id.editprofile_textinputeditextAddAchievements);
        addAchievementsImg= (ImageView) getActivity().findViewById(R.id.editprofile_imageviewAddAchievement);

        saveData.setOnClickListener(this);
        addAchievementsImg.setOnClickListener(this);
    }

    private void setAdapters() {
        civilStatusList= getActivity().getResources().getStringArray(R.array.civil_status);
        workFieldList= getActivity().getResources().getStringArray(R.array.workfield);
        civilStatusadapter= new MaterialSpinnerAdapter(getActivity(),Arrays.asList(civilStatusList));
        workfieldAdapter= new MaterialSpinnerAdapter(getActivity(),Arrays.asList(workFieldList));
        civilStatus.setAdapter(civilStatusadapter);
        workField.setAdapter(workfieldAdapter);
        expandableHeightListView.setExpanded(true);
    }

    private void setSpinnerListeners(){
        civilStatus.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectedCivilStatus= item.toString();
            }
        });

        workField.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectedWorkField= item.toString();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.editprofile_saveData:
//                Save data here
                saveNewData();
                break;
            case R.id.editprofile_imageviewAddAchievement:
                addNewAchievement();
                break;
        }
    }

    private void addNewAchievement(){

        Achievements achievements= new Achievements();
        achievements.setAchievementDetails(addAchievements.getText().toString());
        achievements.setUserUUID(userUUID);

        if (AppUtils.isDeviceConnectedToAnyNetworks(getActivity())) {
            initProgressDialog("Adding data");
            achievementsTransactionPresenter.addNewAchievement(achievements);
        }else {
            Toasty.error(getActivity(),"No internet connection. Please try again",Toast.LENGTH_SHORT,true).show();
        }
    }

    private void saveNewData() {

        Alumni newEditedAlumni= new Alumni();
        newEditedAlumni.setFullName(name.getText().toString());
        newEditedAlumni.setMobileNumber(String.valueOf(mobileNumber.getText()));
        newEditedAlumni.setAddress(address.getText().toString());
        newEditedAlumni.setJobPosition(jobPosition.getText().toString());
        newEditedAlumni.setCompanyName(companyName.getText().toString());
        newEditedAlumni.setCivilStatus(selectedCivilStatus);
        newEditedAlumni.setCurrentJob(selectedWorkField);

        if (AppUtils.isDeviceConnectedToAnyNetworks(getActivity())) {
            initProgressDialog("Updating data");
            editProfilePresenter.saveNewEditedProfile(currentUserData,newEditedAlumni);
            editProfilePresenter.setProfileUpdatecallbackListener(this);
        }else {
            Toasty.error(getActivity(),"No internet connection. Please try again",Toast.LENGTH_SHORT,true).show();
        }


    }

    private int checkItemPosition(String item,String field) {
        int pos=0;

        if (field.equals("cs")) {
            for (String s: civilStatusList){
                if (!s.equals(item)) {
                    pos++;
                }else {
                    selectedCivilStatus=civilStatusList[pos];
                    return pos;
                }
            }
        }else {
            for (String s:workFieldList){
                if (!s.equals(item)){
                    pos++;
                }else {
                    selectedWorkField=workFieldList[pos];
                    return pos;
                }
            }
        }
        return pos;
    }

    private void setWidgetContents(Alumni alumni){
        name.setText(alumni.getFullName());
        address.setText(alumni.getAddress());
        mobileNumber.setText(alumni.getMobileNumber());
        jobPosition.setText(alumni.getJobPosition());
        companyName.setText(alumni.getCompanyName());
        civilStatus.setSelectedIndex(checkItemPosition(alumni.getCivilStatus(),"cs"));
        workField.setSelectedIndex(checkItemPosition(alumni.getCurrentJob(),"csgo"));
    }



    private void updateAchievementList(List<Achievements> achievementsListContent) {
        achievementsAdapter = new AchievementsProfileAdapter(getActivity(),userUUID,achievementsListContent,false);
        expandableHeightListView.setAdapter(achievementsAdapter);
        achievementsAdapter.notifyDataSetChanged();

        achievementsAdapter.setAchievementEditDeleteCallbackListener(this);
    }

    /**
     * ALERT DIALOG FOR EDITING CURRENT SELECTED ACHIEVEMENT
     *
     * @param achievements
     */
    private void showEditAchievementDialog(final Achievements achievements) {

        View view = LayoutInflater.from(v.getContext()).inflate(R.layout.layout_editext_editachievements, null);
        final EditText achievementEditext = view.findViewById(R.id.editext_achievement_edit);
        achievementEditext.setText(achievements.getAchievementDetails());
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext())
                .setMessage("Edit Achievement")
                .setView(view)
                .setPositiveButton("Publish", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        newAchievement = new Achievements();
                        newAchievement.setUuid(achievements.getUuid());
                        newAchievement.setUserUUID(achievements.getUserUUID());
                        newAchievement.setAchievementDetails(achievementEditext.getText().toString());

                        if (!AppUtils.isDeviceConnectedToAnyNetworks(getActivity())) {
                            Toasty.error(getActivity(),"No internet connection. Please try again",Toast.LENGTH_SHORT,true).show();
                        }else {
                            initProgressDialog("Editing data");
                            achievementsTransactionPresenter.editAchievement(achievements, newAchievement);
                        }


                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

    }


    /**
     * ALERT DIALOG FOR DELETING SELECTED ACHIEVEMENT
     *
     * @param achievements
     */

    private void showDeleteDialog(final Achievements achievements) {

        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext())
                .setMessage("Delete this item ?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (AppUtils.isDeviceConnectedToAnyNetworks(getActivity())) {
                            initProgressDialog("Deleting data");
                            achievementsTransactionPresenter.deletenewAnnouncement(achievements.getUuid());
                        }else {
                            Toasty.error(getActivity(),"No internet connection. Please try again",Toast.LENGTH_SHORT,true).show();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void initProgressDialog(String message){

        pd = new ProgressDialog(v.getContext());
        pd.setMessage(message);
        pd.setCanceledOnTouchOutside(false);
        pd.setCancelable(false);
        pd.show();

    }

    ////////////////////////////////////////////////////////////////////////////////////
    ///         CALLBACKS
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * RETRIEVING PROFILE
     * @param alumni
     */
    @Override
    public void successRetrievingProfile(Alumni alumni) {
        currentUserData=new Alumni();
        currentUserData=alumni;
        setWidgetContents(alumni);
    }

    @Override
    public void errorRetrievingProfile(DatabaseError err) {
        Toasty.error(getActivity(),"Error retrieving profile!", Toast.LENGTH_LONG,true).show();
    }


    /**
     * PROFILE EDIT CALLLBACK
     */
    @Override
    public void onEditProfileSuccess() {
        Toasty.success(getActivity(),"Success editing profile",Toast.LENGTH_LONG,true).show();
        pd.dismiss();
        getFragmentManager().popBackStack();
    }

    @Override
    public void onEditProfileFailed() {
        Toasty.error(getActivity(),"Failed editing profile",Toast.LENGTH_LONG,true).show();
        pd.dismiss();
    }


    /**
     * GETTING ACHIEVEMENTS CALLBACK
     * @param achievementsListValue
     */
    @Override
    public void onDataChanged(List<Achievements> achievementsListValue) {
       /* if (!achievementsList.isEmpty()) {
            achievementsList.clear();
        }*/
//        achievementsList=achievementsListValue;
        updateAchievementList(achievementsListValue);
    }


    /**
     * ON EDIT OR DELETE CLICKED IN ACHIEVEMENTS CALLBACK
     * @param toBeEdited
     */
    @Override
    public void onEditSelected(Achievements toBeEdited) {
        Log.e(TAG, "onEditSelected: " );
        showEditAchievementDialog(toBeEdited);
    }

    @Override
    public void onDeleteSelected(Achievements toBeDeleted) {
        Log.e(TAG, "onDeleteSelected: " );
        showDeleteDialog(toBeDeleted);
    }



    /**
     * EDIT ACHIEVEMENT LISTENER
     */
    @Override
    public void onSuccessEdit() {
        pd.dismiss();
    }

    @Override
    public void onFailureEdit() {
        pd.dismiss();
        Toasty.error(getActivity(),"An error occured while editing data",Toast.LENGTH_LONG,true).show();
    }


    /**
     * DELETE ACHIEVEMENT LISTENER
     */
    @Override
    public void onSuccessDelete() {
        pd.dismiss();
    }

    @Override
    public void onFailureDelete() {
        pd.dismiss();
        Toasty.error(getActivity(),"An error occured while deleting data",Toast.LENGTH_LONG,true).show();
    }


    /**
     * ADD ACHIEVEMENTS LISTENER
     */

    @Override
    public void onSuccessAdd() {
        pd.dismiss();
        Toasty.success(getActivity(),"Success adding achievement",Toast.LENGTH_LONG,true).show();
    }

    @Override
    public void onFailureAdd() {
        pd.dismiss();
        Toasty.error(getActivity(),"An error occured while adding data",Toast.LENGTH_LONG,true).show();
    }
}

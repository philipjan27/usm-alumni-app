package usmalumni.emerien.com.usmalumniapp.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import es.dmoral.toasty.Toasty;
import usmalumni.emerien.com.usmalumniapp.R;
import usmalumni.emerien.com.usmalumniapp.authentication.AuthPresenter;
import usmalumni.emerien.com.usmalumniapp.authentication.AuthView;
import usmalumni.emerien.com.usmalumniapp.helpers.AppUtils;
import usmalumni.emerien.com.usmalumniapp.helpers.CoreClass;
import usmalumni.emerien.com.usmalumniapp.helpers.FirebaseUtilities;
import usmalumni.emerien.com.usmalumniapp.helpers.SharedPreferenceHelper;
import usmalumni.emerien.com.usmalumniapp.model.Alumni;
import usmalumni.emerien.com.usmalumniapp.model.Roles;
import usmalumni.emerien.com.usmalumniapp.roles.RolesPresenter;
import usmalumni.emerien.com.usmalumniapp.roles.RolesView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AuthView,RolesView {


    public final static String TAG = MainActivity.class.getSimpleName();

    FirebaseUtilities utils;
    FirebaseUser user;
    FirebaseAuth auth1;
    AuthPresenter presenter;
    RolesPresenter rolesPresenter;
    SharedPreferenceHelper helper;
    SharedPreferences prefs;


    ProgressDialog dialog;
    EditText username, password;
    Button login;
    TextView gotoRegister;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        utils=FirebaseUtilities.getInstance(MainActivity.this);
        utils.checkIfNeededTableExist();
//        utils.addAdminCredentialsToFirebaseDb();

        helper = SharedPreferenceHelper.getInstance();
        prefs = helper.initializePrefs(this);

        auth1 = FirebaseAuth.getInstance();
        presenter = new AuthPresenter(this);
        rolesPresenter= new RolesPresenter(this);

        username = findViewById(R.id.editText_username);
        password = findViewById(R.id.editext_password);
        gotoRegister = findViewById(R.id.tv_register);
        login = findViewById(R.id.buttonlogin);

        dialog = new ProgressDialog(this);
        dialog.setTitle("USM Alumni App");
        dialog.setMessage("Loggin in..");

        gotoRegister.setOnClickListener(this);
        login.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (helper.checkIfLoggedIn(prefs)) {
            if (!helper.getUserUUIDInPrefs(prefs).isEmpty()) {

                if (AppUtils.isDeviceConnectedToAnyNetworks(this)) {
                    presenter.redirectToDashboard(this, helper.getCurrentRoleInPrefs(prefs), helper.getUserUUIDInPrefs(prefs));
                }else {
                    redirectToWifi();
                }

            }else {
                Log.e(TAG, "onResume: USER UUID is EMPTY!");
            }
        }

    }

    @Override
    public void onBackPressed() {
        createExitAlertDialog();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_register:
                gotoRegisterFragment();
                break;
            case R.id.buttonlogin:
                if (AppUtils.isDeviceConnectedToAnyNetworks(this)) {
                    proceedLogin();
                }else {
                    redirectToWifi();
                }
                break;
        }
    }

    private void redirectToWifi() {
        AlertDialog.Builder builder= new AlertDialog.Builder(this)
                .setMessage("No internet connection. Please connect to wifi or mobile data")
                .setPositiveButton("Wifi Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                });

        AlertDialog dialog= builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.show();
    }

    private void proceedLogin() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (TextUtils.isEmpty(username.getText().toString()) || TextUtils.isEmpty(password.getText().toString())) {
                    Toasty.error(MainActivity.this, "Please provide email and password", Toast.LENGTH_LONG, true).show();
                } else {
                    dialog.show();
                    presenter.checkLogin(MainActivity.this, auth1, username.getText().toString(), password.getText().toString());
                }
            }
        });


    }

    private void gotoRegisterFragment() {
        presenter.redirectToRegistration(this);
    }

    @Override
    public void onSuccess(FirebaseAuth auth) {
        presenter.checkIfAlreadyLoggedIn(user, auth);
        dialog.dismiss();
        rolesPresenter.checkLoginRole(username.getText().toString());
    }

    @Override
    public void onError() {
        dialog.dismiss();
    }

    @Override
    public void onFailure() {
        Toasty.error(this,"Incorrect email or password. Please try again.",Toast.LENGTH_LONG,true).show();
    }

    @Override
    public void onAlreadyLoggedIn(FirebaseUser user) {
        helper.setPrefsContent(prefs,user);
    }

    @Override
    public void onNotAlreadyLogon() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void createExitAlertDialog () {
        AlertDialog.Builder builder= new AlertDialog.Builder(this)
                .setTitle("USM Alumni App")
                .setMessage("Are you sure you want to exit ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        System.exit(0);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog= builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public void roleType(String role,String userUUID) {
        Log.e(TAG, "roleType: ROLE: "+role+"\t"+"USERUUID: "+userUUID );
        switch (role) {
            case "Admin":
                helper.saveCurrentRoleToPrefs(prefs,"Admin");
                helper.saveUserUUIDtoPrefs(prefs,userUUID);
                presenter.redirectToDashboard(this,"Admin",userUUID);
                break;
            case "Alumni":
                helper.saveCurrentRoleToPrefs(prefs,"Alumni");
                helper.saveUserUUIDtoPrefs(prefs,userUUID);
                presenter.redirectToDashboard(this,"Alumni",userUUID);
                break;
            case "Faculty":
                helper.saveCurrentRoleToPrefs(prefs,"Faculty");
                helper.saveUserUUIDtoPrefs(prefs,userUUID);
                presenter.redirectToDashboard(this,"Faculty",userUUID);
                break;
        }
    }


}

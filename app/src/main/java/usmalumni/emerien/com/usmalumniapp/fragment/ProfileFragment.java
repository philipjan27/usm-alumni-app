package usmalumni.emerien.com.usmalumniapp.fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.google.firebase.database.DatabaseError;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import usmalumni.emerien.com.usmalumniapp.R;
import usmalumni.emerien.com.usmalumniapp.adapter.AchievementsProfileAdapter;
import usmalumni.emerien.com.usmalumniapp.helpers.SharedPreferenceHelper;
import usmalumni.emerien.com.usmalumniapp.model.Achievements;
import usmalumni.emerien.com.usmalumniapp.model.Alumni;
import usmalumni.emerien.com.usmalumniapp.ownprofile.AchievementsTransactionPresenter;
import usmalumni.emerien.com.usmalumniapp.ownprofile.ProfileRetrievePresenter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements ProfileRetrievePresenter.RetrieveProfileCallback, ProfileRetrievePresenter.RetrieveAchievementsCallbackMethod2, AchievementsProfileAdapter.OnAchievementEditOrDeleteCallbackListener, AchievementsTransactionPresenter.AchievementEditCallback, AchievementsTransactionPresenter.AchievementDeleteCallback {

    public final static String TAG = ProfileFragment.class.getSimpleName();

    /**
     * WIDGETS
     */
    View v;
    ProgressDialog pd;
    FloatingActionButton editProfile;
    CircleImageView circleImageView;
    TextView fullname, email, mobileNumber, course, address, batch, civilStatus, jobPosition, company;
    ExpandableHeightListView accomplishments;

    /**
     * GLOBAL VARIABLES
     */
    String role;
    String uuid;
    SharedPreferenceHelper preferenceHelper;
    SharedPreferences prefs;

    AchievementsTransactionPresenter achievementsTransactionPresenter;
    ProfileRetrievePresenter retrievePresenter;

    /**
     * ACHIEVEMENTS ADAPTER, PRESENTER STUFFS
     */
    Achievements newAchievement;
    AchievementsProfileAdapter achievementsProfileAdapter;
    List<Achievements> achievementsListRef = new ArrayList<>();

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        retrievePresenter = new ProfileRetrievePresenter(this);
        preferenceHelper = SharedPreferenceHelper.getInstance();
        prefs = preferenceHelper.initializePrefs(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_profile, container, false);

        uuid = preferenceHelper.getUserUUIDInPrefs(prefs);
        role = preferenceHelper.getCurrentRoleInPrefs(prefs);


        retrievePresenter.getUserProfile(uuid);
        retrievePresenter.getUserAchievementsAnotherMethod(uuid);
        retrievePresenter.setRetrieveAchievementsMethod2Listener(this);
        Log.e(TAG, "onCreateView: UUID \t" + uuid);
        achievementsProfileAdapter = new AchievementsProfileAdapter(getActivity(), uuid, achievementsListRef,true);
        achievementsTransactionPresenter = new AchievementsTransactionPresenter();
        achievementsTransactionPresenter.setAchievementEditCallbackListeners(this);
        achievementsTransactionPresenter.setAchievementDeleteCallbackListener(this);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        fullname = (TextView) v.findViewById(R.id.profile_fullname);
        email = (TextView) v.findViewById(R.id.profile_emailadd);
        mobileNumber = (TextView) v.findViewById(R.id.profile_mobilenumber);
        course = (TextView) v.findViewById(R.id.profile_course);
        address = (TextView) v.findViewById(R.id.profile_address);
        batch = (TextView) v.findViewById(R.id.profile_batch);
        civilStatus = (TextView) v.findViewById(R.id.profile_civilstatus);
        jobPosition = (TextView) v.findViewById(R.id.profile_jobPosition);
        company = (TextView) v.findViewById(R.id.profile_company);
        editProfile = (FloatingActionButton) v.findViewById(R.id.profile_edit);
        circleImageView = (CircleImageView) v.findViewById(R.id.profile_circularimageview);
        accomplishments = (ExpandableHeightListView) v.findViewById(R.id.profile_expandableheightLv);
        accomplishments.setExpanded(true);
        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG, "onClick: FAB");
                editProfile();
            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * RETRIEVING USER PROFILE CALLBACK
     *
     * @param alumni
     */
    @Override
    public void successRetrievingProfile(Alumni alumni) {
        fullname.setText(alumni.getFullName());
        email.setText(alumni.getEmail());
        mobileNumber.setText(alumni.getMobileNumber());
        course.setText(alumni.getCourse());
        address.setText(alumni.getAddress());
        batch.setText(alumni.getYearBatch());
        civilStatus.setText(alumni.getCivilStatus());
        jobPosition.setText(alumni.getJobPosition());
        company.setText(alumni.getCompanyName());

    /*    if (!alumni.getImageFile().isEmpty() || alumni.getImageFile() != "") {
            if (!alumni.getImageFile().equals(" ")){
                Picasso.with(getActivity()).load(R.drawable.ic_emptyprofilepic).into(circleImageView);
            }else {
                Picasso.with(getActivity()).load(alumni.getImageFile()).into(circleImageView);
            }
        }else {
            Picasso.with(getActivity()).load(R.drawable.ic_emptyprofilepic).into(circleImageView);
        }*/

        if (alumni.getImageFile().isEmpty() || alumni.getImageFile().equals("")) {
//            Picasso.with(getActivity()).load(R.drawable.ic_emptyprofilepic).resize(70, 70).onlyScaleDown().into(circleImageView);
            Picasso.with(getActivity()).load(R.drawable.ic_emptyprofilepic).centerCrop().resize(100,100).into(circleImageView);
        } else {
//            Picasso.with(getActivity()).load(alumni.getImageFile()).resize(70, 70).onlyScaleDown().into(circleImageView);
            Picasso.with(getActivity()).load(alumni.getImageFile()).centerCrop().resize(100,100).into(circleImageView);
        }

    }

    @Override
    public void errorRetrievingProfile(DatabaseError err) {
        Toasty.error(getActivity(), "Error in retrieving profile", Toast.LENGTH_LONG, true).show();
    }

    /**
     * ALERT DIALOG FOR EDITING CURRENT SELECTED ACHIEVEMENT
     *
     * @param achievements
     */
    private void showEditAchievementDialog(final Achievements achievements) {

        pd = new ProgressDialog(v.getContext());
        pd.setMessage("Editing data");
        pd.setCanceledOnTouchOutside(false);
        pd.setCancelable(false);

        View view = LayoutInflater.from(v.getContext()).inflate(R.layout.layout_editext_editachievements, null);
        final EditText achievementEditext = view.findViewById(R.id.editext_achievement_edit);
        achievementEditext.setText(achievements.getAchievementDetails());
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext())
                .setMessage("Edit Achievement")
                .setView(view)
                .setPositiveButton("Publish", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        pd.show();
                        newAchievement = new Achievements();
                        newAchievement.setUuid(achievements.getUuid());
                        newAchievement.setUserUUID(achievements.getUserUUID());
                        newAchievement.setAchievementDetails(achievementEditext.getText().toString());
                        achievementsTransactionPresenter.editAchievement(achievements, newAchievement);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

    }


    /**
     * ALERT DIALOG FOR DELETING SELECTED ACHIEVEMENT
     *
     * @param achievements
     */

    private void showDeleteDialog(final Achievements achievements) {

        pd = new ProgressDialog(v.getContext());
        pd.setMessage("Deleting data");
        pd.setCanceledOnTouchOutside(false);
        pd.setCancelable(false);

        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext())
                .setMessage("Delete this item ?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        pd.show();
                        achievementsTransactionPresenter.deletenewAnnouncement(achievements.getUuid());
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    /**
     * RETRIEVE ACHIEVEMENTS CALLBACK
     *
     * @param achievementsList
     */
    @Override
    public void onDataChanged(List<Achievements> achievementsList) {

        Log.e(TAG, "onDataChanged: FILTERED ACHIEVEMENT LIST SIZE: " + achievementsList.size());

        if (!achievementsListRef.isEmpty()) {
            achievementsListRef.clear();
        }
        achievementsListRef = achievementsList;
        updateAchievementList(achievementsListRef);
    }

    /**
     * UPDATE ACHIEVEMENTS ADAPTER!
     *
     * @param achievementsList
     */
    private void updateAchievementList(List<Achievements> achievementsList) {
        achievementsProfileAdapter = new AchievementsProfileAdapter(getActivity(), uuid, achievementsList,true);
        accomplishments.setAdapter(achievementsProfileAdapter);
        achievementsProfileAdapter.notifyDataSetChanged();
        achievementsProfileAdapter.setAchievementEditDeleteCallbackListener(this);
    }


    /**
     * EDIT OR DELETE BUTTON CALLED CALLBACK
     *
     * @param toBeEdited
     */
    @Override
    public void onEditSelected(Achievements toBeEdited) {
        Log.e(TAG, "onEditSelected: " + toBeEdited.getAchievementDetails());
        showEditAchievementDialog(toBeEdited);
    }

    @Override
    public void onDeleteSelected(Achievements toBeDeleted) {
        Log.e(TAG, "onDeleteSelected: " + toBeDeleted.getAchievementDetails());
        showDeleteDialog(toBeDeleted);
    }


    /**
     * EDIT ACHIEVEMENT CALLBACK
     */
    @Override
    public void onSuccessEdit() {
        updateAchievementList(achievementsListRef);
        pd.dismiss();
        Toasty.success(getActivity(), "Success editing data", Toast.LENGTH_LONG, true).show();
    }

    @Override
    public void onFailureEdit() {
        pd.dismiss();
        Toasty.error(getActivity(), "An error occured while updating data", Toast.LENGTH_LONG, true).show();
    }


    /**
     * DELETE ACHIEVEMENT CALLBACK
     */
    @Override
    public void onSuccessDelete() {
        updateAchievementList(achievementsListRef);
        pd.dismiss();
        Toasty.success(getActivity(), "Success deletion of data", Toast.LENGTH_LONG, true).show();
    }

    @Override
    public void onFailureDelete() {
        pd.dismiss();
        Toasty.error(getActivity(), "An error occured while deleting data", Toast.LENGTH_LONG, true).show();
    }

    private void editProfile() {

        try {
            EditProfileFragment editProfileFragment = new EditProfileFragment();

            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.editprofile_container, editProfileFragment, "editprofile");
            ft.addToBackStack("editprofile");
            ft.commit();

        } catch (Exception ex) {
            Log.e(TAG, "editProfile: " + ex.getMessage());
        }

    }
}
